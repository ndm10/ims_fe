import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ScheduleRoutingModule } from './schedule-routing.module';
import { ListScheduleComponent } from './list-schedule/list-schedule.component';
import { ScheduleDetailsComponent } from './schedule-details/schedule-details.component';
import { UpdateScheduleDetailsComponent } from './update-schedule-details/update-schedule-details.component';
import { UpdateScheduleResultComponent } from './update-schedule-result/update-schedule-result.component';
import { CancelScheduleComponent } from './cancel-schedule/cancel-schedule.component';
import { SharedModule } from '../../shared/shared.module';
import { AddScheduleComponent } from './add-schedule/add-schedule.component';

@NgModule({
  declarations: [
    ListScheduleComponent,
    ScheduleDetailsComponent,
    UpdateScheduleDetailsComponent,
    UpdateScheduleResultComponent,
    CancelScheduleComponent,
    AddScheduleComponent,
  ],
  imports: [CommonModule, ScheduleRoutingModule, SharedModule],
})
export class ScheduleModule {}
