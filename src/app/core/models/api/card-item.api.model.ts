export interface ApiCardItem {
  total: number;
  percentage: number;
}
