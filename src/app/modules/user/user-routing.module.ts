import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListUserComponent } from './list-user/list-user.component';
import { DetailUserComponent } from './detail-user/detail-user.component';
import { AddUserComponent } from './add-user/add-user.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { authGuard } from '../../core/guards/auth.guard';
import { Roles } from '../../../environments/roles';

const routes: Routes = [
  { path: '', redirectTo: 'list-user', pathMatch: 'full' },
  {
    path: 'list-user',
    component: ListUserComponent,
    canActivate: [authGuard],
    data: {
      breadcrumb: 'List Users',
      roles: [Roles.Admin],
    },
  },
  {
    path: 'detail-user/:id',
    component: DetailUserComponent,
    canActivate: [authGuard],
    data: {
      breadcrumb: 'Detail User',
      roles: [Roles.Admin],
    },
  },
  {
    path: 'add-user',
    component: AddUserComponent,
    canActivate: [authGuard],
    data: {
      breadcrumb: 'Create User',
      roles: [Roles.Admin],
    },
  },
  {
    path: 'edit-user/:id',
    component: EditUserComponent,
    canActivate: [authGuard],
    data: {
      breadcrumb: 'Edit User',
      roles: [Roles.Admin],
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserRoutingModule {}
