import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import {
  IDropdownSettings,
  MultiSelectComponent,
} from 'ng-multiselect-dropdown/public_api';
import { __values } from 'tslib';
import { AuthService } from './core/services/auth.service';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { SignalRService } from './core/services/signal-r.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NotificationService } from './core/services/notification.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css',
})
export class AppComponent implements OnInit {
  user: any;
  constructor(
    private authService: AuthService,
    private cookieService: CookieService,
    private signalRService: SignalRService,
    private snackBar: MatSnackBar,
    private router: Router,
    private notificationService: NotificationService
  ) {}
  ngOnInit(): void {
    this.authService.user().subscribe({
      next: (user) => {
        this.user = user;
      },
    });

    // Check if the user is logged in to connect to SignalR
    this.user = this.authService.getUser();
    if (this.user) {
      // Get token from cookie
      const authHeader = this.cookieService.get('Authorization');
      const accessToken = authHeader.split(' ')[1];

      // Connect to SignalR with the token
      this.signalRService
        .startConnectionWithAuthenticate(
          'http://171.244.61.201:7080/notification',
          accessToken
        )
        .then(() => {
          // Now that the connection is established, set up event listeners to notification
          this.signalRService.addListener('Notify', (data) => {
            // Notify the notification service that a new notification has been received
            this.notificationService.notifyNewNotification();
            // Show a snackbar to notify the user
            this.snackBar.open(data, '', {
              duration: 2000,
              horizontalPosition: 'center',
              verticalPosition: 'top',
              panelClass: 'app-notification-sucscess',
            });
          });
        })
        .catch((error) => {
          console.error('Connection failed:', error);
        });
    }
  }

  onLogout() {
    this.authService.logout();
    this.router.navigateByUrl('/login');
  }
}
