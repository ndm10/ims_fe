import {
  Component,
  ElementRef,
  OnInit,
  ViewChild,
  computed,
  model,
  signal,
} from '@angular/core';
import { UIListSchedule } from '../../../core/models/ui/schedule/list-schedule.ui.model';
import { Column } from '../../../core/models/ui/column.ui.model';
import { ScheduleStatusEnum } from '../../../shared/enum/schedule-status.enum';
import { ScheduleService } from '../../../core/services/schedule.service';
import { Router } from '@angular/router';
import { AuthService } from '../../../core/services/auth.service';
import { Roles } from '../../../../environments/roles';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';

@Component({
  selector: 'app-list-schedule',
  templateUrl: './list-schedule.component.html',
  styleUrl: './list-schedule.component.css',
})
export class ListScheduleComponent implements OnInit {
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];

  schedules?: UIListSchedule;

  interviewerNames?: { value: any; label: string }[];
  readonly displayChipInterviewer = model('');
  readonly interviewers = signal([]);

  cols: Column[] = [
    { displayName: 'Title', key: 'title' },
    { displayName: 'Candidate Name', key: 'candidateName' },
    { displayName: 'Interviewer', key: 'interviewerName' },
    { displayName: 'Schedule', key: 'schedule' },
    { displayName: 'Result', key: 'result' },
    { displayName: 'Status', key: 'status' },
    { displayName: 'Job', key: 'jobName' },
  ];
  loading: boolean = true;
  textSearch: string;
  statusSelectValues: ScheduleStatusEnum;
  interviewerSelectValue: string;

  statuses: any[] = Object.values(ScheduleStatusEnum).filter(
    (value) => typeof value === 'number'
  ) as ScheduleStatusEnum[];

  isInterviewer: boolean;

  /**
   *
   */
  constructor(
    private scheduleService: ScheduleService,
    private router: Router,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.isInterviewer = this.IsInterviewer();
    // Call API to get all schedules
    this.scheduleService.getSchedules().subscribe({
      next: (response) => {
        this.schedules = response;
        this.loading = false;
      },
    });

    // Mock data for interviewerNames
    this.interviewerNames = [
      { value: '08dc85fe-de1c-4e2f-82e5-a02971c87a8e', label: 'itvnv1' },
      { value: '08dc8c24-2bca-492e-86d1-c21fb8105658', label: 'itvnv2' },
      { value: '08dc8c24-3251-4508-82e5-e937e070f22d', label: 'itvnv3' },
    ];
  }

  // Filter data for interviewerNames
  readonly filteredInterviewers = computed(() => {
    const currentInterviewer = this.displayChipInterviewer().toLowerCase();
    return currentInterviewer
      ? this.interviewerNames.filter((interviewer) =>
          interviewer.label.toLowerCase().includes(currentInterviewer)
        )
      : this.interviewerNames.slice();
  });

  //#region event handlers for chips and autocomplete inputs for interviewer
  @ViewChild('interviewerInput') interviewerInput: ElementRef;
  isShowPlaceholderInterviewer = true;

  removeInterviewer(interviewerRemove: string) {
    // prevent when there is no interviewer
    if (this.isShowPlaceholderInterviewer) {
      return;
    }

    // remove interviewer
    this.interviewers.update((interviewer) => {
      const index = interviewer.indexOf(interviewerRemove);
      if (index < 0) {
        return interviewer;
      }

      // enable input
      this.isShowPlaceholderInterviewer = true;
      this.interviewerInput.nativeElement.disabled = false;

      interviewer.splice(index, 1);
      this.updateInterviewerSearch('');
      console.log('candidate id' + this.interviewerSelectValue);

      return [...interviewer];
    });
  }

  selectedInterviewer(event: MatAutocompleteSelectedEvent) {
    // prevent when there is exist interviewer
    if (!this.isShowPlaceholderInterviewer) {
      return;
    }

    // add interviewer
    this.interviewers.update((interviewer) => [
      ...interviewer,
      event.option.viewValue,
    ]);
    this.displayChipInterviewer.set('');
    this.interviewerInput.nativeElement.value = '';
    this.interviewerInput.nativeElement.disabled = true;
    this.isShowPlaceholderInterviewer = false;
  }

  selectedCandidateValue(value: any) {
    // prevent when there is exist interviewer
    if (!this.isShowPlaceholderInterviewer) {
      return;
    }
    // set value to form control
    this.updateInterviewerSearch(value);
    // get value from form control
    console.log('candidate id' + this.interviewerSelectValue);
  }
  //#endregion

  handleChangePage(event: { page: number; pageSize: number }) {
    this.scheduleService
      .getSchedules(
        this.textSearch,
        this.statusSelectValues,
        this.interviewerSelectValue,
        event.page,
        event.pageSize
      )
      .subscribe({
        next: (response) => {
          if (response.items.length > 0) this.schedules = response;
          this.loading = false;
        },
      });
  }

  handlePageSizeChange(event: { page: number; pageSize: number }) {
    this.scheduleService
      .getSchedules(
        this.textSearch,
        this.statusSelectValues,
        this.interviewerSelectValue,
        event.page,
        event.pageSize
      )
      .subscribe({
        next: (response) => {
          if (response.items.length > 0) this.schedules = response;
          this.loading = false;
        },
      });
  }

  handleSearch() {
    this.scheduleService
      .getSchedules(
        this.textSearch,
        this.statusSelectValues,
        this.interviewerSelectValue,
        0,
        this.schedules.pageSize
      )
      .subscribe({
        next: (response) => {
          console.log(response);
          this.schedules = response;
          this.loading = false;
        },
      });
  }

  handleRedirectDetail(id: string) {
    this.router.navigate(['/schedule/schedule-details', id]);
  }

  handleRedirectEdit(id: string) {
    let urlRedirect;
    if (this.isInterviewer) urlRedirect = '/schedule/edit-schedule-result';
    else urlRedirect = '/schedule/edit-schedule';
    this.router.navigate([urlRedirect, id]);
  }

  IsInterviewer(): boolean {
    if (this.authService.getUserRole() == Roles.Interviewer) {
      return true;
    }
    return false;
  }

  updateInterviewerSearch(value: any) {
    this.interviewerSelectValue = value;
  }
}
