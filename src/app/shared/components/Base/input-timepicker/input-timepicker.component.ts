import { Component, forwardRef, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-input-timepicker',
  templateUrl: './input-timepicker.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputTimepickerComponent),
      multi: true,
    },
  ],
})
export class InputTimepickerComponent implements ControlValueAccessor {
  @Input() name: string;
  @Input() valueInput: string;

  onChange: any = () => {};
  onTouched: any = () => {};

  writeValue(value: any): void {
    this.valueInput = value;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
}
