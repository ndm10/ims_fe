import { Injectable } from '@angular/core';
import * as signalR from '@microsoft/signalr';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root', // Ensure the service is provided at the root level
})

export class SignalRService {
  public hubConnection: signalR.HubConnection;

  constructor(
  ) {}

  public startConnectionWithAuthenticate = (
    hubUrl: string,
    token: string
  ): Promise<void> => {
    this.hubConnection = new signalR.HubConnectionBuilder()
      .withUrl(hubUrl, {
        accessTokenFactory: () => token,
      })
      .withAutomaticReconnect()
      .build();

    return this.hubConnection
      .start()
      //.then(() => console.log('Connection started'))
      .catch((err) => console.log('Error while starting connection: ' + err));
  };

  public addListener = (
    eventName: string,
    action: (...args: any[]) => void
  ) => {
    this.hubConnection.on(eventName, action);
  };

  // Add other methods as needed
}
