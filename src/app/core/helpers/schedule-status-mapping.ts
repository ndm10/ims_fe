export const ScheduleStatusMapping: { [key: number]: string } = {
    0: 'Open',
    1: 'Invited',
    2: 'Interviewed',
    3: 'Cancelled',
    4: 'Closed',
  };
  