export interface UIAccount {
  id: string;
  fullName: string;
  dateOfBirth: Date;
  address: string;
}
