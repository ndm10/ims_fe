export interface UIPosition {
  id: string;
  name: string;
  description: string;
}
