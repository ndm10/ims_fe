import { HttpErrorResponse, HttpInterceptorFn } from '@angular/common/http';
import { inject } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { catchError, throwError } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { RouteStateService } from '../services/route-state.service';
import { Router } from '@angular/router';
export const AuthInterceptor: HttpInterceptorFn = (req, next) => {
  const cookieService = inject(CookieService);
  const authService = inject(AuthService);
  const routeStateService = inject(RouteStateService);
  const router = inject(Router);
  // const token = localStorage.getItem('Authorization');
  // console.log(token);
  const authRequest = req.clone({
    headers: req.headers.set(
      'Authorization',
      cookieService.get('Authorization')
      // localStorage.getItem('Authorization')
    ),
  });
  return next(authRequest).pipe(
    catchError((error: HttpErrorResponse) => {
      if (error.status === 401) {
        //cập nhật refreshtoken mới
        authService.getRefreshToken();
        // const previousUrl = routeStateService.getPreviousUrl();
        // if (previousUrl) {
        //   router.navigateByUrl(previousUrl);
        // } else {
        //   router.navigate(['/dashboard']);
        // }

        // console.log(error);
      }
      return throwError(error);
    })
  );
};
