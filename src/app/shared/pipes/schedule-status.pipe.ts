import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'scheduleStatus',
})
export class ScheduleStatusPipe implements PipeTransform {
  transform(apiStatus: number): string {
    switch (apiStatus) {
      case 0:
        return 'Open';
      case 1:
        return 'Invited';
      case 2:
        return 'Interviewed';
      case 3:
        return 'Cancelled';
      case 4:
        return 'Closed';
      default:
        return 'Unknown';
    }
  }
}
