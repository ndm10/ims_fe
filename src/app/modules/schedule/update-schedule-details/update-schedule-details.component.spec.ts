import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateScheduleDetailsComponent } from './update-schedule-details.component';

describe('UpdateScheduleDetailsComponent', () => {
  let component: UpdateScheduleDetailsComponent;
  let fixture: ComponentFixture<UpdateScheduleDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UpdateScheduleDetailsComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(UpdateScheduleDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
