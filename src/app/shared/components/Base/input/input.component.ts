import {
  Component,
  Input,
  forwardRef,
  EventEmitter,
  Output,
  OnInit,
  Injector,
  Inject,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Subject } from 'rxjs';

type InputType = 'text' | 'number' | 'email' | 'password';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
  //cung cấp một instance của InputComponent mỗi khi Angular cần một instance của NG_VALUE_ACCESSOR
  //giúp angular hiểu có 1 loại ControlValueAccessor là input component trong projectt,cách nó nhận biết là kiểm tra có token không
  providers: [
    {
      //một token mà Angular sử dụng để tìm kiếm instances của ControlValueAccessor.
      provide: NG_VALUE_ACCESSOR,
      // tham chiếu đến lớp InputComponent,tham chiếu DI
      useExisting: forwardRef(() => InputComponent),
      //nhiều providers được inject với cùng một token
      multi: true,
    },
  ],
})
export class InputComponent implements ControlValueAccessor, OnInit {
  //input
  @Input() inputId = '';
  @Input() label = '';
  @Input() type: InputType = 'text';
  //custom error
  @Input() customErrorMessages: Record<string, string> = {};
  @Input() isHasButton: boolean = false;
  private _destroy$ = new Subject<void>();
  value: string = '';
  //output
  @Output() onClickButtonEmitter = new EventEmitter<string>();
  @Output() onChangeInputEmitter = new EventEmitter<string>();
  @Output() onFocusInputEmitter = new EventEmitter<string>();
  @Output() onBlurInputEmitter = new EventEmitter<string>();
  @Output() onInputInputEmitter = new EventEmitter<string>();
  //cva form control
  // control: FormControl | undefined;

  constructor(@Inject(Injector) private injector: Injector) {}
  ngOnInit(): void {}
  //cva state
  onChange: (value: any) => void;
  onTouched: () => void;
  isDisabled: boolean;
  //cva method
  writeValue(obj: any): void {
    this.value = obj;
  }
  registerOnChange(fn: (value: any) => void): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }

  //custom handle
  handleClickButtonInternal() {
    this.onClickButtonEmitter.emit(this.value);
  }
  handleInputChangeInternal(): void {
    //ghi nhận lại sự thay đổi của value
    this.onChangeInputEmitter.emit(this.value);
  }
  handleInputInputInternal(): void {
    this.onChange(this.value);
    this.onInputInputEmitter.emit(this.value);
  }
  handleBlurInputInternal() {
    this.onBlurInputEmitter.emit(this.value);
  }
  handleFocusInputInternal() {
    this.onFocusInputEmitter.emit(this.value);
  }
}
