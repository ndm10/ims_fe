import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { BaseService } from '../http/base-service';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { jwtDecode } from 'jwt-decode';
import { ApiPaths } from '../../../environments/api-paths';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  $user = new BehaviorSubject<any>(undefined);
  public $refreshToken = new Subject<boolean>();
  public $refreshTokenReceived = new Subject<boolean>();

  constructor(
    private service: BaseService,
    private cookieService: CookieService,
    private router: Router
  ) {
    this.$refreshToken.subscribe((res: any) => {
      this.getRefreshToken();
    });
  }

  login(request: any): Observable<any> {
    return this.service.post<any>(ApiPaths.login, request);
  }

  setUser(token: string): void {
    const decodedToken = jwtDecode(token);
    const email =
      decodedToken[
        'http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress'
      ];
    const role =
      decodedToken[
        'http://schemas.microsoft.com/ws/2008/06/identity/claims/role'
      ];
    const id =
      decodedToken[
        'http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier'
      ];
    const username = decodedToken['username'];
    localStorage.setItem('email', email);
    localStorage.setItem('role', role);
    localStorage.setItem('userId', id);
    localStorage.setItem('username', username);
    this.$user.next({ email: email, role: role });
  }

  user(): Observable<any> {
    return this.$user.asObservable();
  }

  getUser(): any {
    const email = localStorage.getItem('email');
    const role = localStorage.getItem('role');
    const userId = localStorage.getItem('userId');
    const username = localStorage.getItem('username');
    if (email && role) {
      const user = {
        email: email,
        role: role,
        userId: userId,
        username: username,
      };

      return user;
    }

    return undefined;
  }

  getUserRole(): string {
    return localStorage.getItem('role');
  }

  logout(): void {
    localStorage.clear();
    this.cookieService.delete('Authorization', '/');
    this.cookieService.delete('RefreshToken', '/');
    this.$user.next(undefined);
  }

  public getRefreshToken() {
    const obj = {
      token: this.cookieService.get('Authorization').slice(7),
      refreshToken: this.cookieService.get('RefreshToken'),
      // token: localStorage.getItem('Authorization').slice(7),
      // refreshToken: localStorage.getItem('RefreshToken'),
    };
    console.log(obj);
    this.service.post(ApiPaths.getNewToken, obj).subscribe(
      (res: any) => {
        this.cookieService.delete('RefreshToken', '/');
        this.cookieService.delete('Authorization', '/');
        console.log(res);

        this.cookieService.set(
          'Authorization',
          `Bearer ${res.token}`,
          undefined,
          '/',
          undefined,
          false,
          'Strict'
        );
        this.cookieService.set(
          'RefreshToken',
          res.refreshToken,
          undefined,
          '/',
          undefined,
          false,
          'Strict'
        );
        const newToken = {
          token: this.cookieService.get('Authorization').slice(7),
          refreshToken: this.cookieService.get('RefreshToken'),
          // token: localStorage.getItem('Authorization').slice(7),
          // refreshToken: localStorage.getItem('RefreshToken'),
        };
        console.log('new token:' + newToken);
        // localStorage.setItem('Authorization', `Bearer ${res.token}`);
        // localStorage.setItem('RefreshToken', res.refreshToken);
        this.$refreshTokenReceived.next(true);
      },
      (error: any) => {
        localStorage.clear();
        this.cookieService.delete('Authorization', '/');
        this.cookieService.delete('RefreshToken', '/');
        this.router.navigate(['/login']);
      }
    );
  }
  forgotPassword(value: any): Observable<any> {
    return this.service.post(ApiPaths.forgotPassword, value);
  }
  resetPassword(value: any): Observable<any> {
    return this.service.post(ApiPaths.resetPassword, value);
  }
  checkExpiredToken(value: any): Observable<any> {
    return this.service.post(ApiPaths.checkExpiredToken, value);
  }
}
