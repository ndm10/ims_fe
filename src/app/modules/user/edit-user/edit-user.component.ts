import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../../core/services/common.service';
import { UserService } from '../../../core/services/user.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { UserUpdateRequest } from '../../../core/models/api/user/update-user.api.model';
import { ApiUser } from '../../../core/models/api/user/user.api.model';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ApiDepartment } from '../../../core/models/api/department.api.model';
import { UIStatuses } from '../../../core/models/ui/user/status.ui.model';
import { UIGender } from '../../../core/models/ui/user/gender.ui.model';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrl: './edit-user.component.css'
})
export class EditUserComponent implements OnInit  {
  userId: string;
  toSelectDepartments: ApiDepartment[];
  toSelectRoles: string[];
  toSelectStatuses: UIStatuses[];
  toSelectGenders: UIGender[];
  public userRequestModel!: UserUpdateRequest;
  public userDetailsModel : ApiUser;
  public userForm!: FormGroup;
  
  constructor(
    private commonService: CommonService,
    private userService: UserService,
    private snackBar: MatSnackBar,
    private router: Router,
    private route: ActivatedRoute,) {
  }
  ngOnInit(): void {
    this.userId = this.route.snapshot.paramMap.get('id');
    this.createForm();
    this.getRecord();
    this.commonService.getDepartments().subscribe({
      next: (departments) => {
        this.toSelectDepartments = departments;
      },
    });
    this.commonService.getAllRoles().subscribe({
      next: (roles) => {
        this.toSelectRoles = roles;
      },
    });
    this.setSelectStatuses();
    this.setSelectGenders();
  }
  private getRecord() {
    this.userService.getDetail(this.userId).subscribe((data) => {
      if (data) {
        this.userDetailsModel = data;
        this.updateFormValue();
      }
    });
  }
  private createForm(): void {
    this.userForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      fullName: new FormControl('', [Validators.required]),
      dateOfBirth: new FormControl(''),
      address: new FormControl(''),
      gender: new FormControl('', [Validators.required]),
      note: new FormControl(''),
      departmentId: new FormControl('', [Validators.required]),
      roleName: new FormControl('', [Validators.required]),
      status: new FormControl(''),
      phoneNumber:  new FormControl('', [
        Validators.pattern('^[0-9]*$'),
        Validators.minLength(9),
        Validators.maxLength(12)
      ]),
    });
  }
  private updateFormValue() {
    this.userForm.patchValue({
      email: this.userDetailsModel.email,
      fullName:  this.userDetailsModel.fullName,
      dateOfBirth:  this.userDetailsModel.dateOfBirth,
      address: this.userDetailsModel.address,
      gender: this.parseToBoolean(this.userDetailsModel.gender),
      note: this.userDetailsModel.note,
      departmentId:  this.userDetailsModel.departmentId,
      roleName: this.userDetailsModel.roleName,
      phoneNumber: this.userDetailsModel.phoneNumber,
    });
  }
  private parseToBoolean(value: any): boolean {
    return value === 'Activate' || value === true || value === 1;
  }
  private setSelectStatuses(): void {
    this.toSelectStatuses = [
      { label: 'Active', value: true },
      { label: 'Inactive', value: false },
    ];
  }

  private setSelectGenders(): void {
    this.toSelectGenders = [
      { label: 'Male', value: true },
      { label: 'FeMale', value: false },
    ];
  }

  handleCancel() {
    this.router.navigate(['user/list-user']);
  }
  handleSubmit() {
    if (this.userForm.valid) {
      let data = this.userForm.value as UserUpdateRequest;
      //if data of birth is not selected, set it is null
      data.dateOfBirth = data.dateOfBirth || null;
      data.phoneNumber =  data.phoneNumber || null;
      data.id = this.userId;

      this.userService.editUser(data).subscribe(
        (response) => {
          if (response.isSuccess) {
            this.snackBar.open('Edit user successfully', '', {
              duration: 2000,
              horizontalPosition: 'right',
              verticalPosition: 'top',
              panelClass: 'app-notification-success',
            });
            this.router.navigate(['/user/list-user']);
          } else {
            debugger
            this.snackBar.open(response.messageResponse, '', {
              duration: 2000,
              horizontalPosition: 'right',
              verticalPosition: 'top',
              panelClass: 'app-notification-error',
            });
          }
        },
        (error) => {
          let errorMessage = 'An unexpected error occurred';

          if (error.error) {
            if (error.error.message) {
              errorMessage = error.error.message;
            } else if (Array.isArray(error.error.Errors) && error.error.Errors.length > 0) {
              errorMessage = error.error.Errors.join(', ');
            }
          }

          this.snackBar.open(
            errorMessage,
            '',
            {
              duration: 2000,
              horizontalPosition: 'right',
              verticalPosition: 'top',
              panelClass: 'app-notification-error',
            }
          );
        }
      );
    } else {
      console.warn('Form is invalid');
    }
  }
}
