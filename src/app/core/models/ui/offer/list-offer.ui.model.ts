import { UIOffer } from './offer.ui.model';

export interface UIListOffers {
  pageIndex: number;
  totalPages: number;
  pageSize: number;
  totalRecord: number;
  hasPreviousPage: boolean;
  hasNextPage: boolean;
  items: UIOffer[];
}
