export interface ResponseData<T> {
  data: T;
  isSuccess: boolean;
  messageResponse: string;
}
