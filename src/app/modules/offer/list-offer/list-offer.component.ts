import { Component, OnInit } from '@angular/core';
import { OfferService } from '../../../core/services/offer.service';
import { Column } from '../../../core/models/ui/column.ui.model';
import { Router } from '@angular/router';
import { UIListOffers } from '../../../core/models/ui/offer/list-offer.ui.model';
import { MatDialog } from '@angular/material/dialog';
import { ExportOfferExcelDialogComponent } from '../export-offer-excel-dialog/export-offer-excel-dialog.component';
import { saveAs } from 'file-saver';
import { OfferStatus } from '../../../shared/enum/offer-status.enum';
import { CommonService } from '../../../core/services/common.service';
import { formatDate } from '../../../core/helpers/date-only-mapping';
@Component({
  selector: 'app-list-offer',
  templateUrl: './list-offer.component.html',
  styleUrl: './list-offer.component.css',
})
export class ListOfferComponent implements OnInit {
  offers?: UIListOffers;
  cols: Column[] = [
    { displayName: 'Name', key: 'candidateName' },
    { displayName: 'Email', key: 'candidateEmail' },
    { displayName: 'Approver', key: 'approverName' },
    { displayName: 'Department', key: 'departmentName' },
    { displayName: 'Position', key: 'positionName' },
    { displayName: 'Note', key: 'note' },
    { displayName: 'Status', key: 'status' },
  ];
  searchText: string;
  selectedStatus: OfferStatus;
  selectedDepartment: string;
  statuses: any[] = Object.values(OfferStatus).filter(
    (value) => typeof value === 'number'
  ) as OfferStatus[];
  departmentNames: { value: string; label: string }[];
  loading: boolean = true;

  constructor(
    private offerService: OfferService,
    private commonService: CommonService,
    private router: Router,
    private dialog: MatDialog
  ) {}
  ngOnInit(): void {
    this.offerService.getAllOffers().subscribe({
      next: (response) => {
        this.offers = response;

        this.loading = false;
      },
    });
    this.commonService.getDepartments().subscribe({
      next: (departments) => {
        this.departmentNames = departments.map((department) => {
          return {
            value: department.id,
            label: department.name,
          };
        });
      },
    });
  }
  handleChangePage(event: { page: number; pageSize: number }) {
    this.offerService
      .getAllOffers(
        this.searchText,
        this.selectedStatus,
        this.selectedDepartment,
        event.page,
        event.pageSize
      )
      .subscribe({
        next: (response) => {
          this.offers = response;
          this.loading = false;
        },
      });
  }
  handlePageSizeChange(event: { page: number; pageSize: number }) {
    this.offerService
      .getAllOffers(
        this.searchText,
        this.selectedStatus,
        this.selectedDepartment,
        event.page,
        event.pageSize
      )
      .subscribe({
        next: (response) => {
          this.offers = response;
          this.loading = false;
        },
      });
  }
  handleSearch() {
    this.offerService
      .getAllOffers(
        this.searchText,
        this.selectedStatus,
        this.selectedDepartment,
        0,
        this.offers.pageSize
      )
      .subscribe({
        next: (response) => {
          this.offers = response;
          this.loading = false;
        },
      });
  }
  handleRedirectDetail(id: string) {
    this.router.navigate(['offer/detail-offer', id]);
  }
  handleRedirectEdit(id: string) {
    this.router.navigate(['offer/edit-offer', id]);
  }
  openExportDialog() {
    const dialogRef = this.dialog.open(ExportOfferExcelDialogComponent, {
      width: '700px',
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        // Kiểm tra xem kết quả từ dialog có dữ liệu không
        this.offerService.exportOfferExcel(result).subscribe(
          (data: any) => {
            const blob = new Blob([data], {
              type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            });
            const file = new File(
              [blob],
              `offer-list-from:${formatDate(result.fromDate)}to:${formatDate(
                result.toDate
              )}.xlsx`,
              {
                type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
              }
            );
            saveAs(file); // Sử dụng saveAs từ thư viện file-saver để tải file về
          },
          (error) => {
            console.error('Export failed', error);
            // Xử lý thông báo lỗi hoặc hiển thị thông báo cho người dùng
          }
        );
      }
    });
  }
}
