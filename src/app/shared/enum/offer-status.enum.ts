export enum OfferStatus {
  WaitingForApproval = 0,
  Approved,
  Rejected,
  WaitingForResponse,
  Accepted,
  Declined,
  Cancelled,
}
