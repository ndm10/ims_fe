import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../../../core/services/dashboard.service';
import { ApiCardItem } from '../../../core/models/api/card-item.api.model';
import { MatSnackBar } from '@angular/material/snack-bar';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrl: './dashboard.component.css',
})
export class DashboardComponent implements OnInit {
  cardItems: ApiCardItem[] = [
    {
      total: 0,
      percentage: 0,
    },
    {
      total: 0,
      percentage: 0,
    },
    {
      total: 0,
      percentage: 0,
    },
    {
      total: 0,
      percentage: 0,
    },
  ];
  numberInterviews = {
    labels: [],
    datasets: [
      {
        data: [],
        label: 'Number of interviews',
        fill: true,
        backgroundColor: 'rgb(3, 138, 255)',
        borderColor: 'blue',
        tension: 0.4,
      },
    ],
  };

  successInterviews = {
    labels: [],
    datasets: [
      {
        data: [],
        fill: true,
        backgroundColor: ['blue', 'red'],
        borderColor: 'black',
      },
    ],
  };

  constructor(
    private dashboardService: DashboardService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.dashboardService.getCardItems().subscribe(
      (response) => {
        this.cardItems = response.data;
      },
      (error) => {
        this.snackBar.open(error.error.Errors[0], '', {
          duration: 2000,
          horizontalPosition: 'right',
          verticalPosition: 'top',
          panelClass: 'app-notification-error',
        });
      }
    );
    this.countInterviews(2);
    this.successCandidates(2);
  }

  countInterviews(dateType: number) {
    this.dashboardService.countInterviews(dateType).subscribe((response) => {
      if (response.isSuccess) {
        this.numberInterviews = {
          labels: response.data.map((x) => x.label),
          datasets: [
            {
              data: response.data.map((x) => x.value),
              label: 'Number of interviews',
              fill: true,
              backgroundColor: 'rgba(3, 138, 255, 0.5)',
              borderColor: 'blue',
              tension: 0.4,
            },
          ],
        };
      }
    });
  }

  successCandidates(dateType: number) {
    this.dashboardService.successCandidates(dateType).subscribe((response) => {
      if (response.isSuccess) {
        this.successInterviews = {
          labels: response.data.map((x) => x.label),
          datasets: [
            {
              data: response.data.map((x) => x.value),
              fill: true,
              backgroundColor: ['blue', 'red'],
              borderColor: 'black',
            },
          ],
        };
        //console.log(this.successInterviews);
      }
    });
  }
}
