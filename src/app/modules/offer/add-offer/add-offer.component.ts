import { Component, OnInit } from '@angular/core';
import { CandidateService } from '../../../core/services/candidate.service';
import { provideNativeDateAdapter } from '@angular/material/core';
import { Router } from '@angular/router';
import { ContractType } from '../../../shared/enum/contract-type.enum';
import { OfferStatus } from '../../../shared/enum/offer-status.enum';
import { AccountService } from '../../../core/services/account.service';
import { ScheduleService } from '../../../core/services/schedule.service';
import { ApiSchedule } from '../../../core/models/api/schedule/schedule.api.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommonService } from '../../../core/services/common.service';
import { OfferService } from '../../../core/services/offer.service';
import { ApiDepartment } from '../../../core/models/api/department.api.model';
import { AuthService } from '../../../core/services/auth.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { dateLessThan } from '../../../core/helpers/date.validator';
import { futureDateValidator } from '../../../core/helpers/due-date.validator';
import { Roles } from '../../../../environments/roles';

@Component({
  selector: 'app-add-offer',
  templateUrl: './add-offer.component.html',
  styleUrl: './add-offer.component.css',
  providers: [provideNativeDateAdapter()],
})
export class AddOfferComponent implements OnInit {
  //select input candidate name
  candidateNames?: { value: any; label: string }[];
  //select input contract type
  contractTypes = Object.values(ContractType).filter(
    (value) => typeof value === 'number'
  ) as ContractType[];
  //select input approver
  approverNames?: { value: any; label: string }[];
  //select input hr
  hrNames?: { value: any; label: string }[];
  //select input  schedule
  schedules?: ApiSchedule[];
  selectedSchedule?: ApiSchedule;
  //select input department ,position, level
  departmentNames: ApiDepartment[];
  positionNames: { value: string; label: string }[];
  levelNames: { value: string; label: string }[];
  //form control
  offerForm: FormGroup;

  constructor(
    private candidateService: CandidateService,
    private accountService: AccountService,
    private scheduleService: ScheduleService,
    private commonService: CommonService,
    private offerService: OfferService,
    private authService: AuthService,
    private snackBar: MatSnackBar,
    private router: Router,
    private fb: FormBuilder
  ) {}
  ngOnInit(): void {
    this.candidateService.getCandidateWithoutBan().subscribe({
      next: (candidates) => {
        this.candidateNames = candidates.map((candidate) => {
          return {
            value: candidate.id,
            label: candidate.fullName,
          };
        });
      },
    });
    this.accountService.getUserByRole('Recruiter').subscribe({
      next: (hrs) => {
        this.hrNames = hrs.map((hr) => {
          return {
            value: hr.id,
            label: hr.fullName,
          };
        });
      },
    });
    this.accountService.getUserByRole('Manager').subscribe({
      next: (managers) => {
        this.approverNames = managers.map((manager) => {
          return {
            value: manager.id,
            label: manager.fullName,
          };
        });
      },
    });
    this.commonService.getPositions().subscribe({
      next: (positions) => {
        this.positionNames = positions.map((positions) => {
          return {
            value: positions.id,
            label: positions.name,
          };
        });
      },
    });
    this.commonService.getDepartments().subscribe({
      next: (departments) => {
        this.departmentNames = departments;
      },
    });
    this.commonService.getAttributes(2).subscribe({
      next: (levels) => {
        this.levelNames = levels.map((level) => {
          return {
            value: level.id,
            label: level.value,
          };
        });
      },
    });
    this.offerForm = this.fb.group(
      {
        candidateId: ['', Validators.required], // Tạo các FormControl và ánh xạ với các trường trong form
        positionId: ['', Validators.required],
        managerId: ['', Validators.required],
        scheduleId: [null],
        contractPeriodStart: ['', Validators.required],
        contractPeriodEnd: ['', Validators.required],
        contractType: ['', Validators.required],
        level: ['', Validators.required],
        departmentId: ['', Validators.required],
        recruiterId: ['', Validators.required],
        dueDate: ['', Validators.required],
        basicSalary: ['', Validators.required],
        note: [''],
      },
      {
        validators: [
          dateLessThan('contractPeriodStart', 'contractPeriodEnd'),
          futureDateValidator('dueDate'),
        ],
      }
    );
  }
  handleChangeCandidate(id: any) {
    this.scheduleService.getScheduleByCandidateId(id).subscribe({
      next: (schedules) => {
        this.schedules = schedules;
      },
    });
  }
  handleChangeSchedule(id: any) {
    this.selectedSchedule = this.schedules.filter(
      (schedule) => schedule.id == id
    )[0];
  }
  handleSubmit() {
    console.log(this.offerForm.value);
    if (this.offerForm.valid) {
      const formData = this.offerForm.value;
      this.offerService.addOffer(formData).subscribe(
        (response) => {
          if (response.isSuccess) {
            this.snackBar.open('Add offer successfully', '', {
              duration: 2000,
              horizontalPosition: 'right',
              verticalPosition: 'top',
              panelClass: 'app-notification-success',
            });
            this.router.navigate(['/offer/list-offer']);
          } else {
            this.snackBar.open(response.messageResponse, '', {
              duration: 2000,
              horizontalPosition: 'right',
              verticalPosition: 'top',
              panelClass: 'app-notification-error',
            });
          }
        },
        (error) => {
          this.snackBar.open(
            error.error.message ?? error.error.errors[0].errors[0].errorMessage,
            '',
            {
              duration: 2000,
              horizontalPosition: 'right',
              verticalPosition: 'top',
              panelClass: 'app-notification-error',
            }
          );
        }
      );
    } else {
      console.warn(this.offerForm.valid);
    }
  }
  handleCancel() {
    this.router.navigate(['offer/list-offer']);
  }
  assignMe() {
    const id = this.authService.getUser().userId;
    if (this.authService.getUserRole() == Roles.HR) {
      this.offerForm.patchValue({
        recruiterId: id,
      });
    } else {
      this.snackBar.open("Your role can't assign", '', {
        duration: 2000,
        horizontalPosition: 'right',
        verticalPosition: 'top',
        panelClass: 'app-notification-error',
      });
    }
  }
  isDateLessThanError(): boolean {
    const form = this.offerForm;
    return (
      form.errors &&
      form.errors['dateLessThan'] &&
      form.get('contractPeriodStart')?.touched &&
      form.get('contractPeriodEnd')?.touched
    );
  }
  isFutureDateError(): boolean {
    const form = this.offerForm;
    const dueDateControl = form.get('dueDate');
    return form?.errors?.['futureDate'] && dueDateControl.touched;
  }
}
