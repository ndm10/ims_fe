import { UICandidate } from './candidate.ui.model';

export interface UIListCadidates {
  pageIndex: number;
  totalPages: number;
  pageSize: number;
  totalRecord: number;
  hasPreviousPage: boolean;
  hasNextPage: boolean;
  items: UICandidate[];
}
