export interface CheckExpiredTokenRequest {
    ResetPasswordToken: string;
    Email: string;
}