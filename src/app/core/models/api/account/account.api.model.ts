export interface ApiAccount {
  id: string;
  fullName: string;
  dateOfBirth: Date;
  address: string;
  gender: boolean;
  note: null;
}
