export interface UIAttribute {
  id: string;
  value: string;
}
