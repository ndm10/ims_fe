import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-single-select-box',
  templateUrl: './single-select-box.component.html',
  styleUrl: './single-select-box.component.css',
})
export class SingleSelectBoxComponent {
  @Input() options: { value: any; label: string }[];
  @Input() value: any;
  @Output() valueChange = new EventEmitter<any>();

  onChange(event: Event) {
    this.valueChange.emit((event.target as HTMLSelectElement).value);
  }
}
