import { UIAttribute } from '../attribute.ui.model';

export interface UICandidate {
  accountName: string;
  address: string;
  skills: UIAttribute[];
  highestLevel: UIAttribute;
  createdAt: Date;
  createdBy: string;
  dob: string;
  email: string;
  fullName: string;
  gender: string;
  id: string;
  note: string;
  phoneNumber: string;
  positionName: string;
  resume: string;
  status: string;
  updateAt: Date;
  updateBy: string;
  yoe: number;
}
