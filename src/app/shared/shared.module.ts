import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableComponent } from './components/Base/table/table.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OfferStatusPipe } from './pipes/offer-status.pipe';
import { CurrencyFormatPipe } from './pipes/money-format.pipe';
import { SingleSelectBoxComponent } from './components/Base/single-select-box/single-select-box.component';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatIconModule } from '@angular/material/icon';
import { ContractTypePipe } from './pipes/contract-type.pipe';
import { BreadcrumbComponent } from './components/Layout/breadcrumb/breadcrumb.component';
import { RouterModule } from '@angular/router';
import { CandidateStatusPipe } from './pipes/candidate-status.pipe';
import { GenderPipe } from './pipes/gender.pipe';
import { MatDialogModule } from '@angular/material/dialog';
import { ConfirmDialogComponent } from './components/Dialog/confirm-dialog/confirm-dialog.component';
import { CustomSnackbarComponent } from './components/Dialog/custom-snackbar/custom-snackbar.component';
import { MatTooltipModule } from '@angular/material/tooltip';
import { HeaderComponent } from './components/Layout/header/header.component';
import { SidebarComponent } from './components/Layout/sidebar/sidebar.component';
import { AdminlayoutComponent } from './components/Layout/adminlayout/adminlayout.component';
import { InputComponent } from './components/Base/input/input.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { NotFoundComponent } from './components/Page/not-found/not-found.component';
import { NoPermissionComponent } from './components/Page/no-permission/no-permission.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatChipsModule } from '@angular/material/chips';
import { NgChartsModule } from 'ng2-charts';
import { InputTimepickerComponent } from './components/Base/input-timepicker/input-timepicker.component';
import { ScheduleStatusPipe } from './pipes/schedule-status.pipe';
import { ScheduleResultPipe } from './pipes/schedule-result.pipe';
import {MatMenuModule} from '@angular/material/menu';
import { MailConfirmComponent } from './components/Page/mail-confirm/mail-confirm.component';
import { NotificationComponent } from './components/Layout/notification/notification.component';
import {MatDividerModule} from '@angular/material/divider';
import { ExpiredResetPasswordTokenComponent } from './components/Page/expired-reset-password-token/expired-reset-password-token.component';

@NgModule({
  declarations: [
    TableComponent,
    NotFoundComponent,
    NoPermissionComponent,
    InputComponent,
    TableComponent,
    OfferStatusPipe,
    ScheduleStatusPipe,
    ContractTypePipe,
    CurrencyFormatPipe,
    CandidateStatusPipe,
    GenderPipe,
    SingleSelectBoxComponent,
    BreadcrumbComponent,
    HeaderComponent,
    SidebarComponent,
    AdminlayoutComponent,
    ConfirmDialogComponent,
    CustomSnackbarComponent,
    NotFoundComponent,
    NoPermissionComponent,
    InputTimepickerComponent,
    ScheduleResultPipe,
    MailConfirmComponent,
    NotificationComponent,
    ExpiredResetPasswordTokenComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    ReactiveFormsModule,
    RouterModule,
    MatDialogModule,
    MatDialogModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    MatAutocompleteModule,
    MatChipsModule,
    NgChartsModule,
    MatMenuModule,
    MatDividerModule,
  ],
  exports: [
    TableComponent,
    NotFoundComponent,
    NoPermissionComponent,
    OfferStatusPipe,
    ScheduleStatusPipe,
    MailConfirmComponent,
    CurrencyFormatPipe,
    CandidateStatusPipe,
    ContractTypePipe,
    GenderPipe,
    SingleSelectBoxComponent,
    FormsModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatFormFieldModule,
    MatDialogModule,
    MatDatepickerModule,
    MatSelectModule,
    MatIconModule,
    ReactiveFormsModule,
    AdminlayoutComponent,
    MatDialogModule,
    MatAutocompleteModule,
    MatChipsModule,
    NgChartsModule,
    InputTimepickerComponent,
    ScheduleResultPipe,
    MatMenuModule,
    MatDividerModule,
    NotificationComponent,
  ],
})
export class SharedModule {}
