import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpiredResetPasswordTokenComponent } from './expired-reset-password-token.component';

describe('ExpiredResetPasswordTokenComponent', () => {
  let component: ExpiredResetPasswordTokenComponent;
  let fixture: ComponentFixture<ExpiredResetPasswordTokenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ExpiredResetPasswordTokenComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ExpiredResetPasswordTokenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
