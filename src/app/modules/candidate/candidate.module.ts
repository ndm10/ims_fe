import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CandidateRoutingModule } from './candidate-routing.module';
import { ListCandidateComponent } from './list-candidate/list-candidate.component';
import { SharedModule } from '../../shared/shared.module';
import { DetailCandidateComponent } from './detail-candidate/detail-candidate.component';
import { AddCandidateComponent } from './add-candidate/add-candidate.component';
import { EditCandidateComponent } from './edit-candidate/edit-candidate.component';

@NgModule({
  declarations: [
    ListCandidateComponent,
    DetailCandidateComponent,
    AddCandidateComponent,
    EditCandidateComponent,
  ],
  imports: [CommonModule, CandidateRoutingModule, SharedModule],
})
export class CandidateModule {}
