export enum AttributeEnum {
  HighestLevel = 1,
  Level = 2,
  Skills = 3,
  Benefit = 4,
}
