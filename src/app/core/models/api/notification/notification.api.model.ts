export interface ApiNotification {
  id: string;
  title: string;
  content: string;
  toUserId: string;
  urlRedirect: Date;
  isRead: boolean;
  createdAt: Date;
}
