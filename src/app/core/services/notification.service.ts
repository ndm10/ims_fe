import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, Observable, map } from 'rxjs';
import { BaseService } from '../http/base-service';
import { ResponseData } from '../models/api/response.api.model';
import { ApiListNotification } from '../models/api/notification/list-notification.api.model';
import { ApiPaths } from '../../../environments/api-paths';
import { UIListNotification } from '../models/ui/notification/list-notification.ui.model';
import { formatDateTime } from '../helpers/date-time-mapping';

@Injectable({
  providedIn: 'root',
})
export class NotificationService {
  private isAllReadSource = new BehaviorSubject<boolean>(null);
  currentIsAllRead = this.isAllReadSource.asObservable();

  private newNotificationSource = new BehaviorSubject<boolean>(null);
  newNotification$ = this.newNotificationSource.asObservable();

  constructor(
    private snackBar: MatSnackBar,
    private baseService: BaseService
  ) {}

  notifyNewNotification() {
    this.newNotificationSource.next(true);
  }

  showSnackbar(message: string): void {
    this.snackBar.open(message, 'Close', {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
    });
  }

  changeIsAllRead(value: boolean) {
    this.isAllReadSource.next(value);
  }

  // get notifications
  getNotifications(
    pageIndex?: number,
    pageSize?: number
  ): Observable<UIListNotification> {
    let searchUrl = '';
    const params: string[] = [];

    if (pageIndex !== undefined) {
      params.push(`pageIndex=${pageIndex}`);
    }
    if (pageSize !== undefined) {
      params.push(`pageSize=${pageSize}`);
    }

    if (params.length > 0) {
      searchUrl = `?${params.join('&')}`;
    }

    return this.baseService
      .get<ResponseData<ApiListNotification>>(
        ApiPaths.getNotifications(searchUrl)
      )
      .pipe(
        map((response) => {
          if (response.isSuccess) {
            const apiListNotification = response.data;
            const uiNotification = response.data.items.map((notification) => {
              const { createdAt, ...otherProps } = notification;
              return {
                ...otherProps,
                // format date to mm/dd/yyyy hh:mm:ss format
                createdAt: formatDateTime(createdAt),
              };
            });
            return {
              hasPreviousPage: apiListNotification.hasPreviousPage,
              hasNextPage: apiListNotification.hasNextPage,
              pageIndex: apiListNotification.pageIndex,
              totalPages: apiListNotification.totalPages,
              pageSize: apiListNotification.pageSize,
              totalRecord: apiListNotification.totalRecord,
              items: uiNotification,
            };
          } else {
            throw new Error(response.messageResponse);
          }
        })
      );
  }

  // update schedule
  markAsReadNotification(notifiId: string): Observable<any> {
    return this.baseService.put<any>(
      ApiPaths.markAsReadNotificationUrl(notifiId)
    );
  }
}
