export interface Column {
  displayName: string;
  key: string;
}
