import { ActivatedRouteSnapshot, CanActivateFn, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { inject } from '@angular/core';

export const authGuard: CanActivateFn = (
  route: ActivatedRouteSnapshot,
  state
) => {
  const authService = inject(AuthService);
  const router = inject(Router);
  const userRole = authService.getUserRole();
  const requiredRole = route.data['roles'];
  const user = authService.getUser();
  if (user) {
    if (requiredRole && !requiredRole.some((role) => userRole == role)) {
      console.log(true);
      router.navigate(['/no-permission']);
      return false;
    }
    return true;
  } else {
    router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
    return false;
  }
};
