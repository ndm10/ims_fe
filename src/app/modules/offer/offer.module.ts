import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OfferRoutingModule } from './offer-routing.module';
import { ListOfferComponent } from './list-offer/list-offer.component';
import { SharedModule } from '../../shared/shared.module';
import { DetailOfferComponent } from './detail-offer/detail-offer.component';
import { AddOfferComponent } from './add-offer/add-offer.component';
import { EditOfferComponent } from './edit-offer/edit-offer.component';
import { ExportOfferExcelDialogComponent } from './export-offer-excel-dialog/export-offer-excel-dialog.component';
@NgModule({
  declarations: [
    ListOfferComponent,
    DetailOfferComponent,
    AddOfferComponent,
    EditOfferComponent,
    ExportOfferExcelDialogComponent,
  ],

  imports: [CommonModule, OfferRoutingModule, SharedModule],
})
export class OfferModule {}
