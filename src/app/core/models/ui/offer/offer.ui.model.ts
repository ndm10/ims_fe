export interface UIOffer {
  id: string;
  candidateId: string;
  recruiterId: string;
  managerId: string;
  scheduleId: string;
  positionId: string;
  contractPeriodStart: Date;
  contractPeriodEnd: Date;
  contractType: number;
  departmentId: string;
  dueDate: Date;
  basicSalary: number;
  status: string;
  note?: string;
  candidateName: string;
  candidateEmail: string;
  approverName: string;
  departmentName: string;
  positionName: string;
}
