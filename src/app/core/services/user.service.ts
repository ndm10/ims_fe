import { Injectable } from '@angular/core';
import { BaseService } from '../http/base-service';
import { Observable, map } from 'rxjs';
import { ApiListUsers } from '../models/api/user/list-user.api.model';
import { ResponseData } from '../models/api/response.api.model';
import { ApiUser } from '../models/api/user/user.api.model';
import { ApiPaths } from '../../../environments/api-paths';
import { UserCreateRequest } from '../models/api/user/create-user.api.model';
import { UserUpdateRequest } from '../models/api/user/update-user.api.model';
import { LockUnlockRequest } from '../models/api/user/lock-unlock.request.model';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private service: BaseService) {}
  getAllUsers(
    keyword?: string,
    roleName?: string,
    pageIndex?: number,
    pageSize?: number
  ): Observable<ApiListUsers> {
    let url = 'api/user/user-list';
    const params: string[] = [];

    if (keyword !== undefined) {
      params.push(`keyword=${encodeURIComponent(keyword)}`);
    }
    if (roleName !== undefined) {
      params.push(`roleName=${roleName}`);
    }
    if (pageIndex !== undefined) {
      params.push(`pageIndex=${pageIndex}`);
    }
    if (pageSize !== undefined) {
      params.push(`pageSize=${pageSize}`);
    }

    if (params.length > 0) {
      url += '?' + params.join('&');
    }

    return this.service
      .get<ResponseData<ApiListUsers>>(url)
      .pipe(map((response) => response.data));
  }
  getDetail(id: string): Observable<ApiUser> {
    return this.service
      .get<ResponseData<ApiUser>>(ApiPaths.getUserById(id))
      .pipe(
        map((response) => {
          if (response.isSuccess) {
            return response.data;
          } else {
            throw new Error(response.messageResponse);
          }
        })
      );
  }
  addUser(userData: UserCreateRequest): Observable<ResponseData<ApiUser>> {
    const url = ApiPaths.createUser;
    return this.service
      .post<ResponseData<ApiUser>>(url, userData)
      .pipe(map((response) => response));
  }
  editUser(userData: UserUpdateRequest): Observable<ResponseData<ApiUser>> {
    const url = ApiPaths.editUser;
    return this.service
      .put<ResponseData<ApiUser>>(url, userData)
      .pipe(map((response) => response));
  }
  lockUnlockUser(
    userData: LockUnlockRequest
  ): Observable<ResponseData<ApiUser>> {
    const url = ApiPaths.lockUnlock;
    return this.service
      .post<ResponseData<ApiUser>>(url, userData)
      .pipe(map((response) => response));
  }

  getUserByRoleAndUsername(
    search: string,
    role: string,
    pageIndex: number,
    pageSize: number
  ): Observable<ApiListUsers> {
    let url = 'api/user/get-users-not-ban-by-role-and-username';
    let params = [];
    if (search !== undefined && search !== '') {
      params.push(`textSearch=${search}`);
    }
    if (role !== undefined && role !== '') {
      params.push(`role=${role}`);
    }
    if (pageIndex !== undefined && pageIndex > 0) {
      params.push(`pageIndex=${pageIndex}`);
    }
    if (pageSize !== undefined && pageSize > 0) {
      params.push(`pageSize=${pageSize}`);
    }
    // debugger;
    if (params.length > 0) {
      url += `?${params.join('&')}`;
    }

    return this.service
      .get<ResponseData<ApiListUsers>>(url)
      .pipe(map((response) => response.data));
  }
}
