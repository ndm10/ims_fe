export const StatusMapping: { [key: number]: string } = {
  0: 'Waiting for Approval',
  1: 'Approved',
  2: 'Rejected',
  3: 'Waiting for Response',
  4: 'Accepted Offer',
  5: 'Declined Offer',
  6: 'Cancelled',
};
