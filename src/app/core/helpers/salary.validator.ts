import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export function salaryLessThan(
  salaryMin: string,
  salaryMax: string
): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const min = Number(control.get(salaryMin)?.value);
    const max = Number(control.get(salaryMax)?.value);

    if (min && max && min >= max) {
      return { salaryLessThan: true };
    } else {
      return null;
    }
  };
}
export function salarySameNullOrRequire(
  salaryMin: string,
  salaryMax: string
): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const min = Number(control.get(salaryMin)?.value);
    const max = Number(control.get(salaryMax)?.value);

    if ((min && !max) || (!min && max)) {
      return { salarySameNullOrRequireValid: true };
    } else {
      return null;
    }
  };
}

export function preventEKey(event: KeyboardEvent): void {
  var invalidChars = ['-', '+', 'e', 'ArrowUp', 'ArrowDown'];

  if (invalidChars.includes(event.key)) {
    event.preventDefault();
  }
}
