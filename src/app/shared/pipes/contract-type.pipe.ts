import { Pipe, PipeTransform } from '@angular/core';
import { ContractType } from '../enum/contract-type.enum';

@Pipe({
  name: 'contractType',
})
export class ContractTypePipe implements PipeTransform {
  transform(apiStatus: ContractType): string {
    switch (apiStatus) {
      case ContractType.TrialTwoMonths:
        return 'Trial Two Months';
      case ContractType.TraineeThreeMonths:
        return 'Trainee Three Months';
      case ContractType.OneYear:
        return 'One Year';
      case ContractType.ThreeYears:
        return 'Three Years';
      case ContractType.Unlimited:
        return 'Unlimited';
      default:
        return 'Unknown';
    }
  }
}
