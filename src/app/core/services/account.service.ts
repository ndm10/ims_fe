import { Injectable } from '@angular/core';
import { BaseService } from '../http/base-service';
import { ApiAccount } from '../models/api/account/account.api.model';
import { Observable, map } from 'rxjs';
import { ResponseData } from '../models/api/response.api.model';

@Injectable({
  providedIn: 'root',
})
export class AccountService {
  constructor(private service: BaseService) {}

  //get user by role
  getUserByRole(roleName: string): Observable<ApiAccount[]> {
    return this.service
      .get<ResponseData<ApiAccount[]>>(`api/user/role?roleName=${roleName}`)
      .pipe(
        map((response) => {
          if (response.isSuccess) {
            return response.data;
          } else {
            throw new Error(response.messageResponse);
          }
        })
      );
  }
}
