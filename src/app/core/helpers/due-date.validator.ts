import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export function futureDateValidator(dueDate: string): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const date = control.get(dueDate)?.value;
    const today = new Date();
    today.setHours(0, 0, 0, 0);

    return date > today ? null : { futureDate: true };
  };
}
