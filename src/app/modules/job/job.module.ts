import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JobRoutingModule } from './job-routing.module';
import { ListJobComponent } from './list-job/list-job.component';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DetailJobComponent } from './detail-job/detail-job.component';
import { AddJobComponent } from './add-job/add-job.component';
import { BrowserModule } from '@angular/platform-browser';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatInputModule } from '@angular/material/input';
import { MatNativeDateModule } from '@angular/material/core';
import { UpdateJobComponent } from './update-job/update-job.component';
import { ImportJobComponent } from './import-job/import-job.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
@NgModule({
  declarations: [
    ListJobComponent,
    DetailJobComponent,
    AddJobComponent,
    UpdateJobComponent,
    ImportJobComponent,
  ],
  imports: [
    CommonModule,
    JobRoutingModule,
    FormsModule,
    SharedModule,
    // BrowserModule,
    MatDatepickerModule,
    MatInputModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    MatCardModule,
    MatFormFieldModule,
    MatButtonModule,
    MatFormFieldModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatDividerModule,
    MatListModule,
  ],
})
export class JobModule {}
