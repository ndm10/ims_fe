export interface ApiSchedule {
  id: string;
  title: string;
  jobId: string;
  candidateId: string;
  scheduleDate: Date;
  startTime: string;
  endTime: string;
  location?: string;
  recruiterOwnerId: string;
  note?: string;
  meetingId?: string;
  result?: boolean;
  status?: number;
  createdAt: Date;
  createdBy: string;
  updatedAt: Date;
  updatedBy: string;
  job: {
    id: string;
    title: string;
  };
  candidate: {
    id: string;
    fullName: string;
  };
  recruiterOwner: {
    id: string;
    fullName: string;
    userName: string;
  };
  interviewers: {
    id: string;
    fullName: string;
    userName: string;
  }[];
  createdByUser: {
    id: string;
    fullName: string;
    userName: string;
  };
  updatedByUser: {
    id: string;
    fullName: string;
    userName: string;
  };
}
