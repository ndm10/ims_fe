export interface UIJobItem {
  id: string;
  title: string;
  skills: string;
  benefits: string;
  levels: string;
  startDate: Date;
  endDate: Date;
  salaryMin: number;
  salaryMax: number;
  address: string;
  description: string;
  status: number;
  createdAt: Date;
  createdBy: string;
  updateAt: Date;
  updateBy: string;
}
