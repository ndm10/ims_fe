import { ApiNotification } from './notification.api.model';

export interface ApiListNotification {
  pageIndex: number;
  totalPages: number;
  pageSize: number;
  totalRecord: number;
  hasPreviousPage: boolean;
  hasNextPage: boolean; 
  items: ApiNotification[];
}
