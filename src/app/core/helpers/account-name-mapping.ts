export function formatAccountName(value: string): string {
  return value.split('@')[0];
}
