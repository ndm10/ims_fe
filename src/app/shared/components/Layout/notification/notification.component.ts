import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { NotificationService } from '../../../../core/services/notification.service';
import { UIListNotification } from '../../../../core/models/ui/notification/list-notification.ui.model';
import { UINotification } from '../../../../core/models/ui/notification/notification.ui.model';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrl: './notification.component.css',
})
export class NotificationComponent implements OnInit {
  isAllRead?: boolean;
  notifications: UIListNotification;
  loading: boolean = true;
  notificaionItems: UINotification[];
  isHaveNotification: boolean;

  /**
   *
   */
  constructor(private notificationService: NotificationService) {}

  ngOnInit(): void {
    // Subscribe to the currentIsAllRead BehaviorSubject
    this.notificationService.currentIsAllRead.subscribe(
      (currentValue) => (this.isAllRead = currentValue)
    );

    // Refresh notification list when new notification is received
    this.notificationService.newNotification$.subscribe((newNotification) => {
      if (newNotification) {
        // Get all notifications
        this.GetNewNotification();
      }
    });

    // Get all notifications
    this.GetNewNotification();
  }

  ReadAllNotify() {
    this.isAllRead = true;
  }

  MarkAsRead(notifiId: string) {
    this.notificationService.markAsReadNotification(notifiId).subscribe();
  }

  GetNewNotification() {
    // Get all notifications
    this.notificationService.getNotifications().subscribe({
      next: (response) => {
        this.notifications = response;
        this.notificaionItems = this.notifications.items;

        // If all notifications are read, set isAllRead to true
        this.isAllRead = this.notifications.items.every((item) => item.isRead);

        // If there are no notifications, set isHaveNotification to false
        this.isHaveNotification = this.notifications.items.length > 0;

        this.loading = false;
      },
    });
  }
}
