import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-cancel-schedule',
  templateUrl: './cancel-schedule.component.html',
  styleUrl: './cancel-schedule.component.css',
})
export class CancelScheduleComponent {
  /**
   *
   */
  constructor(public dialogRef: MatDialogRef<CancelScheduleComponent>) {}

  closeDialog() {
    this.dialogRef.close();
  }
  cancelInterview() {
    console.log('Interview Cancelled');
    this.dialogRef.close();
  }
}
