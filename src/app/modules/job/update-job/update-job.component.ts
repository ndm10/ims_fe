import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UIJob } from '../../../core/models/ui/job/job.ui.model';
import { JobService } from '../../../core/services/job.service';
import { CommonService } from '../../../core/services/common.service';
import { AttributeEnum } from '../../../shared/enum/attribute.enum';
import { UIAttribute } from '../../../core/models/ui/attribute.ui.model';
import { JobStatusMapping } from '../../../core/helpers/job-status-mapping';
import { Location } from '@angular/common';
import { dateLessThan } from '../../../core/helpers/date.validator';
import {
  salaryLessThan,
  salarySameNullOrRequire,
  preventEKey,
} from '../../../core/helpers/salary.validator';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-update-job',
  templateUrl: './update-job.component.html',
  styleUrl: './update-job.component.css',
})
export class UpdateJobComponent {
  updateForm: FormGroup;
  private id: string;
  public status: string;
  public jobNeedUpdate: UIJob;
  public skills: UIAttribute[];
  public levels: UIAttribute[];
  public benefits: UIAttribute[];
  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private jobService: JobService,
    private commonService: CommonService,
    private location: Location,
    private snackBar: MatSnackBar
  ) {
    this.commonService
      .getAttributes(AttributeEnum.Skills)
      .subscribe((response) => {
        console.log(response);
        this.skills = response;
      });
    this.commonService
      .getAttributes(AttributeEnum.Level)
      .subscribe((response) => {
        this.levels = response;
      });
    this.commonService
      .getAttributes(AttributeEnum.Benefit)
      .subscribe((response) => {
        this.benefits = response;
      });
    this.updateForm = this.fb.group(
      {
        id: ['', Validators.required],
        title: ['', Validators.required],
        skills: [[], Validators.required],
        benefits: [[], Validators.required],
        levels: [[], Validators.required],
        startDate: ['', Validators.required],
        endDate: ['', Validators.required],
        salaryMin: ['', Validators.min(1)],
        salaryMax: ['', Validators.min(1)],
        address: [''],
        description: [''],
        status: ['', Validators.required],
      },
      {
        validators: [
          dateLessThan('startDate', 'endDate'),
          salaryLessThan('salaryMin', 'salaryMax'),
          salarySameNullOrRequire('salaryMin', 'salaryMax'),
        ],
      }
    );
  }
  preventEKey(event: KeyboardEvent) {
    preventEKey(event);
  }
  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.jobService.getDetail(this.id).subscribe((job) => {
      this.status = job.status.toString();
      this.updateForm.patchValue({
        ...job,
        salaryMin: job.salaryMin.toString(),
        salaryMax: job.salaryMax.toString(),
        skills: job.skills.map((x) => x.id),
        levels: job.levels.map((x) => x.id),
        benefits: job.benefits.map((x) => x.id),
      });
      console.log(this.updateForm);
    });
  }

  handleSubmit(): void {
    console.log(this.updateForm.value);
    if (this.updateForm.valid) {
      this.jobNeedUpdate = {
        ...this.updateForm.value,
        status:
          this.updateForm.value.status === 'Draft'
            ? 0
            : this.updateForm.value.status === 'Open'
            ? 1
            : 2,
      };

      this.jobService.editJob(this.jobNeedUpdate).subscribe(
        (response) => {
          this.snackBar.open('Update successful', 'Success', {
            duration: 2000,
            horizontalPosition: 'right',
            verticalPosition: 'top',
            panelClass: 'app-notification-success',
          });
          this.router.navigate(['/job/list-job']);
        },
        (error) => {
          this.snackBar.open(error.error.Errors[0], 'Fail', {
            duration: 2000,
            horizontalPosition: 'right',
            verticalPosition: 'top',
            panelClass: 'app-notification-error',
          });
        }
      );
    } else {
      console.warn('Form is invalid');
    }
  }

  handleCancel() {
    this.router.navigate(['/job/list-job']);
  }

  isDateLessThanError(): boolean {
    return (
      new Date(this.updateForm.get('startDate').value) >=
      new Date(this.updateForm.get('endDate').value)
    );
  }

  isSalaryLessThanError(): boolean {
    const form = this.updateForm;
    return form.errors && form.errors['salaryLessThan'];
  }

  isSalarySameNullOrRequire(): boolean {
    const form = this.updateForm;
    return (
      (form.get('salaryMin').value !== null &&
        form.get('salaryMax').value === null) ||
      (form.get('salaryMin').value === null &&
        form.get('salaryMax').value !== null)
    );
  }
}
