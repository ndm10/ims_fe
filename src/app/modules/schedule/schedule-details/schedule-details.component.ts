import { Component, OnInit } from '@angular/core';
import { ApiSchedule } from '../../../core/models/api/schedule/schedule.api.model';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { ScheduleService } from '../../../core/services/schedule.service';
import { Roles } from '../../../../environments/roles';
import { AuthService } from '../../../core/services/auth.service';

@Component({
  selector: 'app-schedule-details',
  templateUrl: './schedule-details.component.html',
  styleUrl: './schedule-details.component.css',
})
export class ScheduleDetailsComponent implements OnInit {
  id: string;
  schedule?: ApiSchedule;
  interviewers: string;
  isInterviewer: boolean;
  /**
   *
   */

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private scheduleService: ScheduleService,
    private location: Location,
    private authService: AuthService
  ) {}

  ngOnInit(): void {

    this.id = this.route.snapshot.paramMap.get('id');

    // Check if user is interviewer
    this.isInterviewer = this.IsInterviewer();

    // Call API to get schedule detail
    this.scheduleService.getDetail(this.id).subscribe({
      next: (res) => {
        this.schedule = res;
        this.interviewers = this.schedule.interviewers
          .map((interviewer) => interviewer.userName)
          .join(', ');
      },
      error: (err) => {
        this.router.navigate(['/not-found']);
      },
    });
  }

  // Redirect to previous page
  goBack() {
    this.location.back();
  }

  // Redirect to edit schedule page
  handleRedirectEdit() {
    let urlRedirect;
    if (this.isInterviewer) urlRedirect = '/schedule/edit-schedule-result';
    else urlRedirect = '/schedule/edit-schedule';
    this.router.navigate([urlRedirect, this.id]);
  }

  // Check if user is interviewer
  IsInterviewer(): boolean {
    if (this.authService.getUserRole() == Roles.Interviewer) {
      return true;
    }
    return false;
  }
}
