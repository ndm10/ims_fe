export const JobStatusMapping: { [key: number]: string } = {
  0: 'Draft',
  1: 'Open',
  2: 'Close',
};
