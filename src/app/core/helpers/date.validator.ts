import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export function dateLessThan(startDate: string, endDate: string): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const start = control.get(startDate)?.value;
    const end = control.get(endDate)?.value;
    if (start && end && start >= end) {
      return { dateLessThan: true };
    }
    return null;
  };
}
export function pastDateValidator(endDate: string): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const date = control.get(endDate)?.value;
    const today = new Date();
    today.setHours(0, 0, 0, 0);

    return date < today ? null : { pastDate: true };
  };
}
