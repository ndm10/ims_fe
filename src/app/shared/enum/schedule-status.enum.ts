export enum ScheduleStatusEnum {
  Open,
  Invited,
  Interviewed,
  Cancelled,
  Closed,
}
