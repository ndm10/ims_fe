import { Component } from '@angular/core';
import { JobService } from '../../../core/services/job.service';
import { UIJobCreate } from '../../../core/models/ui/job/job-create.ui.model';
import { JobStatusMapping } from '../../../core/helpers/job-status-mapping';
import { UIAttribute } from '../../../core/models/ui/attribute.ui.model';
import { CommonService } from '../../../core/services/common.service';
import { AttributeEnum } from '../../../shared/enum/attribute.enum';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { dateLessThan } from '../../../core/helpers/date.validator';
import {
  preventEKey,
  salaryLessThan,
  salarySameNullOrRequire,
} from '../../../core/helpers/salary.validator';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-add-job',
  templateUrl: './add-job.component.html',
  styleUrl: './add-job.component.css',
})
export class AddJobComponent {
  job: UIJobCreate;
  statuses: { id: number; name: string }[] = Object.keys(JobStatusMapping).map(
    (key) => {
      return {
        id: parseInt(key),
        name: JobStatusMapping[parseInt(key)],
      };
    }
  );
  skills: UIAttribute[];
  levels: UIAttribute[];
  benefits: UIAttribute[];

  constructor(
    private jobService: JobService,
    private commonService: CommonService,
    private router: Router,
    private fb: FormBuilder,
    private snackBar: MatSnackBar
  ) {}

  jobForm: FormGroup;
  ngOnInit(): void {
    this.commonService
      .getAttributes(AttributeEnum.Skills)
      .subscribe((response) => {
        console.log(response);
        this.skills = response;
      });
    this.commonService
      .getAttributes(AttributeEnum.Level)
      .subscribe((response) => {
        this.levels = response;
      });
    this.commonService
      .getAttributes(AttributeEnum.Benefit)
      .subscribe((response) => {
        this.benefits = response;
      });
    this.jobForm = this.fb.group(
      {
        title: ['', Validators.required],
        skills: [[], Validators.required],
        benefits: [[], Validators.required],
        levels: [[], Validators.required],
        startDate: ['', Validators.required],
        endDate: ['', Validators.required],
        salaryMin: ['', Validators.min(1)],
        salaryMax: ['', Validators.min(1)],
        address: [''],
        description: [''],
      },
      {
        validators: [
          dateLessThan('startDate', 'endDate'),
          salaryLessThan('salaryMin', 'salaryMax'),
          salarySameNullOrRequire('salaryMin', 'salaryMax'),
        ],
      }
    );
  }
  handleSubmit(): void {
    if (this.jobForm.valid) {
      const formData = this.jobForm.value;
      // debugger;
      this.jobService.addJob(formData).subscribe(
        (response) => {
          this.snackBar.open('Add successful', 'Success', {
            duration: 2000,
            horizontalPosition: 'right',
            verticalPosition: 'top',
            panelClass: 'app-notification-success',
          });
          this.router.navigate(['job/list-job']);
        },
        (error) => {
          this.snackBar.open(error.error.Errors[0], 'Fail', {
            duration: 2000,
            horizontalPosition: 'right',
            verticalPosition: 'top',
            panelClass: 'app-notification-error',
          });
        }
      );
    } else {
      console.log(this.jobForm.errors);
    }
  }

  handleCancel() {
    this.router.navigate(['job/list-job']);
  }

  preventEKey(event: KeyboardEvent) {
    preventEKey(event);
  }

  isDateLessThanError(): boolean {
    const form = this.jobForm;
    return (
      form.errors &&
      form.errors['dateLessThan'] &&
      form.get('startDate')?.touched &&
      form.get('endDate')?.touched
    );
  }

  isSalaryLessThanError(): boolean {
    const form = this.jobForm;
    return (
      form.errors &&
      form.errors['salaryLessThan'] &&
      form.get('salaryMin')?.touched &&
      form.get('salaryMax')?.touched
    );
  }

  isSalarySameNullOrRequire(): boolean {
    const form = this.jobForm;
    return (
      form.errors &&
      form.errors['salarySameNullOrRequireValid'] &&
      form.get('salaryMin')?.touched &&
      form.get('salaryMax')?.touched
    );
  }
}
