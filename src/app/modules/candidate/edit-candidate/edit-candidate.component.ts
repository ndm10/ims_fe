import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GenderMapping } from '../../../core/helpers/gender-mapping';
import { CandidateStatusMapping } from '../../../core/helpers/candidate-status-mapping';
import { UIPosition } from '../../../core/models/ui/position.ui.model';
import { UIAttribute } from '../../../core/models/ui/attribute.ui.model';
import { CandidateService } from '../../../core/services/candidate.service';
import { CommonService } from '../../../core/services/common.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AccountService } from '../../../core/services/account.service';
import { AttributeEnum } from '../../../shared/enum/attribute.enum';
import { provideNativeDateAdapter } from '@angular/material/core';
import { formatAccountName } from '../../../core/helpers/account-name-mapping';
import { stringToFile } from '../../../core/helpers/file-mapping';
import { ApiAttribute } from '../../../core/models/api/attribute.api.model';
import { ApiPosition } from '../../../core/models/api/position.api.model';
import { ApiAccount } from '../../../core/models/api/account/account.api.model';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from '../../../core/services/auth.service';
import { Roles } from '../../../../environments/roles';

@Component({
  selector: 'app-edit-candidate',
  templateUrl: './edit-candidate.component.html',
  styleUrl: './edit-candidate.component.css',
  providers: [provideNativeDateAdapter()],
})
export class EditCandidateComponent implements OnInit {
  id: string;
  candidateForm: FormGroup;
  genders: { id: number; name: string }[] = Object.keys(GenderMapping).map(
    (key) => {
      return {
        id: parseInt(key),
        name: GenderMapping[parseInt(key)],
      };
    }
  );
  statuses: { id: number; name: string }[] = Object.keys(
    CandidateStatusMapping
  ).map((key) => {
    return {
      id: parseInt(key),
      name: CandidateStatusMapping[parseInt(key)],
    };
  });
  positions: ApiPosition[];
  skills: ApiAttribute[];
  highestLevels: ApiAttribute[];
  accounts: ApiAccount[];
  constructor(
    private route: ActivatedRoute,
    private candidateService: CandidateService,
    private accountService: AccountService,
    private commonService: CommonService,
    private authService: AuthService,
    private router: Router,
    private fb: FormBuilder,
    private snackBar: MatSnackBar
  ) {}
  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.candidateForm = this.fb.group({
      id: [this.id],
      fullName: ['', Validators.required],
      dob: ['', Validators.required],
      phoneNumber: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      address: ['', Validators.required],
      gender: [0, Validators.required],
      resume: ['', Validators.required],
      nameResume: ['', Validators.required],
      accountId: ['', Validators.required],
      skillsId: [[], Validators.required],
      positionId: ['', Validators.required],
      note: [''],
      status: [0, Validators.required],
      yoe: [0],
      highestLevelId: ['', Validators.required],
    });
    this.commonService.getPositions().subscribe((response) => {
      this.positions = response;
    });
    this.commonService
      .getAttributes(AttributeEnum.Skills)
      .subscribe((response) => {
        this.skills = response;
      });
    this.commonService
      .getAttributes(AttributeEnum.HighestLevel)
      .subscribe((response) => {
        this.highestLevels = response;
      });
    this.accountService.getUserByRole('Recruiter').subscribe((response) => {
      this.accounts = response;
    });
    this.candidateService.getDetail(this.id).subscribe((response) => {
      this.candidateForm.patchValue(response);
      this.candidateForm.patchValue({
        skillsId: response.skills.map((x) => x.id),
        positionId: response.position.id,
        highestLevelId: response.highestLevel.id,
        accountId: response.ownerHR.id,
      });
      if (response.resume !== undefined && response.resume.length > 0) {
        let fileName = `CV-${formatAccountName(response.email)}.pdf`;
        this.candidateForm.patchValue({
          nameResume: fileName,
          resume: stringToFile(response.resume, fileName),
        });
      }
    });
  }

  handleFileInputChange(fileInput: HTMLInputElement): void {
    if (fileInput.files && fileInput.files.length > 0) {
      this.candidateForm.patchValue({ resume: fileInput.files[0] });
      this.candidateForm.patchValue({ nameResume: fileInput.files[0].name });
    } else {
      this.candidateForm.patchValue({ resume: '' });
      this.candidateForm.patchValue({ nameResume: '' });
    }
  }

  handleSubmit() {
    if (this.candidateForm.valid) {
      this.candidateService.editCandidate(this.candidateForm.value).subscribe(
        (response) => {
          if (response.isSuccess) {
            this.snackBar.open(response.data, '', {
              duration: 2000,
              horizontalPosition: 'right',
              verticalPosition: 'top',
              panelClass: 'app-notification-success',
            });
          } else {
            this.snackBar.open(response.messageResponse, '', {
              duration: 2000,
              horizontalPosition: 'right',
              verticalPosition: 'top',
              panelClass: 'app-notification-error',
            });
          }
        },
        (error) => {
          this.snackBar.open(error.error.Errors[0], '', {
            duration: 2000,
            horizontalPosition: 'right',
            verticalPosition: 'top',
            panelClass: 'app-notification-error',
          });
        }
      );
    } else {
      console.warn('Form is invalid');
    }
  }
  handleCancel() {
    this.router.navigate(['/list-candidate']);
  }

  assignMe() {
    const id = this.authService.getUser().userId;
    if (this.authService.getUserRole() == Roles.HR) {
      this.candidateForm.patchValue({
        accountId: id,
      });
    } else {
      this.snackBar.open("Your role can't assign", '', {
        duration: 2000,
        horizontalPosition: 'right',
        verticalPosition: 'top',
        panelClass: 'app-notification-error',
      });
    }
  }
}
