import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { JobService } from '../../../core/services/job.service';
import { UIJob } from '../../../core/models/ui/job/job.ui.model';
import { ApiAttribute } from '../../../core/models/api/attribute.api.model';
import { AttributeEnum } from '../../../shared/enum/attribute.enum';
import { UIJobItem } from '../../../core/models/ui/job/job-item.ui.model';
import { Roles } from '../../../../environments/roles';
import { AuthService } from '../../../core/services/auth.service';
@Component({
  selector: 'app-detail-job',
  templateUrl: './detail-job.component.html',
  styleUrl: './detail-job.component.css',
})
export class DetailJobComponent {
  constructor(
    private route: ActivatedRoute,
    private jobService: JobService,
    private router: Router,
    private authService: AuthService
  ) {}

  id: string;
  job: UIJobItem;
  userRole: string;
  roles = {
    admin: Roles.Admin,
    hr: Roles.HR,
    manager: Roles.Manager,
    interview: Roles.Interviewer,
  };
  ngOnInit() {
    this.userRole = this.authService.getUserRole();
    this.id = this.route.snapshot.paramMap.get('id');
    this.jobService.getDetail(this.id).subscribe({
      next: (response) => {
        console.log(response);
        this.job = {
          id: response.id,
          title: response.title,
          skills: response.skills.map((attr) => attr.value).join(', '),
          benefits: response.benefits.map((attr) => attr.value).join(', '),
          levels: response.levels.map((attr) => attr.value).join(', '),
          startDate: response.startDate,
          endDate: response.endDate,
          salaryMin: response.salaryMin,
          salaryMax: response.salaryMax,
          address: response.address,
          description: response.description,
          status: response.status,
          createdAt: response.createdAt,
          createdBy: response.createdBy,
          updateAt: response.updateAt,
          updateBy: response.updateBy,
        };
      },
    });
  }

  formatDate(dateString) {
    // Tạo một đối tượng Date từ chuỗi ngày tháng
    const date = new Date(dateString);

    // Lấy ngày, tháng và năm từ đối tượng Date
    const day = String(date.getDate()).padStart(2, '0');
    const month = String(date.getMonth() + 1).padStart(2, '0'); // Tháng tính từ 0-11 nên cần +1
    const year = date.getFullYear();

    // Trả về chuỗi ngày tháng theo định dạng dd/mm/yyyy
    return `${day}/${month}/${year}`;
  }

  formatCurrency(amount) {
    // Chuyển đổi số thành chuỗi và thêm dấu chấm phân cách
    const formattedAmount = amount
      .toString()
      .replace(/\B(?=(\d{3})+(?!\d))/g, '.');

    // Thêm VND vào cuối chuỗi
    const formattedString = `${formattedAmount}VND`;

    return formattedString;
  }

  handleCancel(): void {
    this.router.navigate(['/job/list-job']);
  }
  redirectDetailItem(): void {
    this.router.navigate(['/job/edit-job', this.id]);
  }
}
