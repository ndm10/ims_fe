export interface UICreateCandidate {
  accountId: string;
  address: string;
  skillsId: string[];
  highestLevelId: string;
  createdAt: Date;
  createdBy: string;
  dob: string;
  email: string;
  fullName: string;
  gender: number;
  id: string;
  note: string;
  phoneNumber: string;
  positionId: string;
  resume: string;
  status: number;
  updateAt: Date;
  updateBy: string;
  yoe: number;
}
