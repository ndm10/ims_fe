import { Component } from '@angular/core';
import { ApiSchedule } from '../../../core/models/api/schedule/schedule.api.model';
import { ScheduleService } from '../../../core/services/schedule.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-update-schedule-result',
  templateUrl: './update-schedule-result.component.html',
  styleUrl: './update-schedule-result.component.css',
})
export class UpdateScheduleResultComponent {
  scheduleId: string;
  schedule?: ApiSchedule;
  interviewers: string;
  scheduleForm: FormGroup;

  /**
   *
   */
  constructor(
    private scheduleService: ScheduleService,
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.scheduleId = this.route.snapshot.paramMap.get('id');

    this.scheduleService.getDetail(this.scheduleId).subscribe({
      next: (res) => {
        this.schedule = res;
        this.interviewers = this.schedule.interviewers
          .map((interviewer) => interviewer.userName)
          .join(', ');
      },
      error: (err) => {
        this.router.navigate(['/not-found']);
      },
    });

    // Form submit to update schedule result
    this.scheduleForm = new FormGroup({
      id: new FormControl(this.scheduleId),
      note: new FormControl(null),
      result: new FormControl(null, [Validators.required]),
    });
  }

  goBack() {
    this.location.back();
  }

  handleUpdateResult() {
    if (this.scheduleForm.valid) {
      const formData = this.scheduleForm.value;
      this.scheduleService.updateScheduleResult(formData).subscribe(
        (response) => {
          if (response.isSuccess) {
            this.snackBar.open('Update schedule result successfully', '', {
              duration: 2000,
              horizontalPosition: 'right',
              verticalPosition: 'top',
              panelClass: 'app-notification-success',
            });
            this.router.navigate(['/schedule/schedule-details', this.scheduleId]);
          } else {
            this.snackBar.open(response.messageResponse, '', {
              duration: 2000,
              horizontalPosition: 'right',
              verticalPosition: 'top',
              panelClass: 'app-notification-error',
            });
          }
        },
        (error) => {
          let errorMessage =
            error.error.messageResponse ?? 'An unexpected error occurred.';
          if (error.error.errors && error.error.errors.length > 0) {
            // Assuming there's only one error per field for simplicity
            error.error.errors.forEach((fieldError: any) => {
              const fieldName = fieldError.key; // The field name in the form
              const fieldErrorMessage = fieldError.errors[0].errorMessage; // The error message for the field

              // Check if the form has the field
              if (this.scheduleForm.controls[fieldName]) {
                // Set the error message to the form control
                this.scheduleForm.controls[fieldName].setErrors({
                  serverError: fieldErrorMessage,
                });
              }

              // Update the general error message to show the first error
              errorMessage = fieldErrorMessage;
            });
          }

          // Show the error message in the snackBar
          this.snackBar.open(errorMessage, '', {
            duration: 2000,
            horizontalPosition: 'right',
            verticalPosition: 'top',
            panelClass: 'app-notification-error',
          });
        }
      );
    } else {
      console.warn('Form is invalid');
      // Display invalid form message
      this.snackBar.open('Please fill in the required fields', '', {
        duration: 2000,
        horizontalPosition: 'right',
        verticalPosition: 'top',
        panelClass: 'app-notification-error',
      });
    }
  }
}
