import { Component } from '@angular/core';
import { CandidateService } from '../../../core/services/candidate.service';
import { provideNativeDateAdapter } from '@angular/material/core';
import { GenderMapping } from '../../../core/helpers/gender-mapping';
import { CommonService } from '../../../core/services/common.service';
import { AttributeEnum } from '../../../shared/enum/attribute.enum';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CandidateStatusMapping } from '../../../core/helpers/candidate-status-mapping';
import { AccountService } from '../../../core/services/account.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from '../../../core/services/auth.service';
import { Roles } from '../../../../environments/roles';

@Component({
  selector: 'app-add-candidate',
  templateUrl: './add-candidate.component.html',
  styleUrl: './add-candidate.component.css',
  providers: [provideNativeDateAdapter()],
})
export class AddCandidateComponent {
  candidateForm: FormGroup;
  genders: { id: number; name: string }[] = Object.keys(GenderMapping).map(
    (key) => {
      return {
        id: parseInt(key),
        name: GenderMapping[parseInt(key)],
      };
    }
  );
  statuses: { id: number; name: string }[] = Object.keys(
    CandidateStatusMapping
  ).map((key) => {
    return {
      id: parseInt(key),
      name: CandidateStatusMapping[parseInt(key)],
    };
  });
  positions: any;
  skills: any;
  highestLevels: any;
  accounts: any;
  constructor(
    private candidateService: CandidateService,
    private commonService: CommonService,
    private accountService: AccountService,
    private router: Router,
    private fb: FormBuilder,
    private authService: AuthService,
    private snackBar: MatSnackBar
  ) {}
  ngOnInit(): void {
    this.candidateForm = this.fb.group({
      fullName: ['', Validators.required],
      dob: ['', Validators.required],
      phoneNumber: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      address: ['', Validators.required],
      gender: [0, Validators.required],
      resume: ['', Validators.required],
      nameResume: ['', Validators.required],
      accountId: ['', Validators.required],
      skillsId: [[], Validators.required],
      positionId: ['', Validators.required],
      note: [''],
      status: [1, Validators.required],
      yoe: [0],
      highestLevelId: ['', Validators.required],
    });
    this.commonService.getPositions().subscribe((response) => {
      this.positions = response;
    });
    this.commonService
      .getAttributes(AttributeEnum.Skills)
      .subscribe((response) => {
        this.skills = response;
      });
    this.commonService
      .getAttributes(AttributeEnum.HighestLevel)
      .subscribe((response) => {
        this.highestLevels = response;
      });
    this.accountService.getUserByRole('Recruiter').subscribe((response) => {
      this.accounts = response;
    });
  }

  handleFileInputChange(fileInput: HTMLInputElement): void {
    if (fileInput.files && fileInput.files.length > 0) {
      this.candidateForm.patchValue({ resume: fileInput.files[0] });
      this.candidateForm.patchValue({ nameResume: fileInput.files[0].name });
    } else {
      this.candidateForm.patchValue({ resume: '' });
      this.candidateForm.patchValue({ nameResume: '' });
    }
  }

  handleSubmit() {
    if (this.candidateForm.valid) {
      const formData = this.candidateForm.value;
      this.candidateService.addCandidate(formData).subscribe(
        (response) => {
          if (response.isSuccess) {
            this.snackBar.open('Add candidate successfully', '', {
              duration: 2000,
              horizontalPosition: 'right',
              verticalPosition: 'top',
              panelClass: 'app-notification-success',
            });
            this.router.navigate(['/candidate/list-candidate']);
          } else {
            this.snackBar.open(response.messageResponse, '', {
              duration: 2000,
              horizontalPosition: 'right',
              verticalPosition: 'top',
              panelClass: 'app-notification-error',
            });
          }
        },
        (error) => {
          console.log(error);
          this.snackBar.open(error.error.Errors[0].messageResponse, '', {
            duration: 2000,
            horizontalPosition: 'right',
            verticalPosition: 'top',
            panelClass: 'app-notification-error',
          });
        }
      );
    } else {
      console.warn('Form is invalid');
    }
  }
  handleCancel() {
    this.router.navigate(['/candidate/list-candidate']);
  }

  assignMe() {
    const id = this.authService.getUser().userId;
    if (this.authService.getUserRole() == Roles.HR) {
      this.candidateForm.patchValue({
        accountId: id,
      });
    } else {
      this.snackBar.open("Your role can't assign", '', {
        duration: 2000,
        horizontalPosition: 'right',
        verticalPosition: 'top',
        panelClass: 'app-notification-error',
      });
    }
  }
}
