import { Component, Input } from '@angular/core';
import { ValidationErrors } from '@angular/forms';

@Component({
  selector: 'app-validation-errors',
  templateUrl: './validation-errors.component.html',
  styleUrl: './validation-errors.component.css',
})
export class ValidationErrorsComponent {
  @Input() errors: Record<string, ValidationErrors> | null = {};
  errorMessage: Record<string, string> | null = {
    required: 'This field is required',
  };
}
