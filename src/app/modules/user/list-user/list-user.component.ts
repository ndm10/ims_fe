import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../core/services/user.service';
import { ApiListUsers } from '../../../core/models/api/user/list-user.api.model';
import { Column } from '../../../core/models/ui/column.ui.model';
import { CommonService } from '../../../core/services/common.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrl: './list-user.component.css',
})
export class ListUserComponent implements OnInit {
  constructor(
    private userService: UserService,
    private commonService: CommonService,
    private router: Router
  ) {}
  users?: ApiListUsers;
  cols: Column[] = [
    { displayName: 'UserName', key: 'userName' },
    { displayName: 'Email', key: 'email' },
    { displayName: 'Phone No.', key: 'phoneNumber' },
    { displayName: 'Role', key: 'roleName' },
    { displayName: 'Status', key: 'status' },
  ];
  searchText: string;
  selectedRole: string;

  roleNames: { value: string; label: string }[];
  loading: boolean = true;
  ngOnInit(): void {
    this.userService.getAllUsers().subscribe({
      next: (response) => {
        this.users = response;
        this.loading = false;
      },
    });
    this.commonService.getAllRoles().subscribe({
      next: (roles) => {
        this.roleNames = roles.map((role) => {
          return {
            value: role,
            label: role,
          };
        });
      },
    });
  }
  handleChangePage(event: { page: number; pageSize: number }) {
    this.userService
      .getAllUsers(
        this.searchText,
        this.selectedRole,
        event.page,
        event.pageSize
      )
      .subscribe({
        next: (response) => {
          if (response.items.length > 0) this.users = response;
          this.loading = false;
        },
      });
  }
  handlePageSizeChange(event: { page: number; pageSize: number }) {
    this.userService
      .getAllUsers(
        this.searchText,
        this.selectedRole,
        event.page,
        event.pageSize
      )
      .subscribe({
        next: (response) => {
          if (response.items.length > 0) this.users = response;
          this.loading = false;
        },
      });
  }
  handleSearch() {
    this.userService
      .getAllUsers(
        this.searchText,
        this.selectedRole,
        this.users.pageIndex,
        this.users.pageSize
      )
      .subscribe({
        next: (response) => {
          if (response.items.length > 0) this.users = response;
          this.loading = false;
        },
      });
  }
  handleRedirectDetail(id: string) {
    this.router.navigate(['user/detail-user', id]);
  }
  handleRedirectEdit(id: string) {
    this.router.navigate(['user/edit-user', id]);
  }
}
