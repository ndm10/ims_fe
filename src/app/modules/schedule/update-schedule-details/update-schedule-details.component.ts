import { COMMA, ENTER } from '@angular/cdk/keycodes';

import {
  Component,
  ElementRef,
  OnInit,
  ViewChild,
  computed,
  model,
  signal,
} from '@angular/core';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { Location } from '@angular/common';
import {
  MatOptionSelectionChange,
  provideNativeDateAdapter,
} from '@angular/material/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { ScheduleService } from '../../../core/services/schedule.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../core/services/auth.service';
import { Roles } from '../../../../environments/roles';
import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';
import { ApiSchedule } from '../../../core/models/api/schedule/schedule.api.model';
import { ScheduleStatusEnum } from '../../../shared/enum/schedule-status.enum';
import { CancelScheduleComponent } from '../cancel-schedule/cancel-schedule.component';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from '../../../shared/components/Dialog/confirm-dialog/confirm-dialog.component';
import { JobService } from '../../../core/services/job.service';
import { UserService } from '../../../core/services/user.service';
import { CandidateService } from '../../../core/services/candidate.service';
import { CandidateStatus } from '../../../shared/enum/candidate-status.enum';

// Custom validator function to check array is not empty
export function arrayNotEmptyValidator(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const value = control.value;
    if (value.length > 0) {
      return null; // validation passes, array is not empty
    } else {
      return { arrayNotEmpty: true }; // validation fails, array is empty
    }
  };
}

@Component({
  selector: 'app-update-schedule-details',
  templateUrl: './update-schedule-details.component.html',
  styleUrl: './update-schedule-details.component.css',
  providers: [provideNativeDateAdapter()],
})
export class UpdateScheduleDetailsComponent {
  scheduleId: string;
  scheduleForm: FormGroup;
  scheduleFormCancel: FormGroup;
  schedule?: ApiSchedule;

  isCancelAble: boolean;

  // Define options for dropdowns
  jobNames?: { value: any; label: string }[] = [];
  interviewerNames?: { value: any; label: string }[] = [];
  RecruiterNames?: { value: any; label: string }[] = [];
  candidateNames?: { value: any; label: string }[] = [];

  titileValue: string = '';
  locationValue: string = '';
  meetingIdValue: string = '';

  readonly startDate = new Date();

  //#region chips and autocomplete inputs variables for job, interviewer, recruiter, candidate
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  readonly displayChipJob = model('');
  readonly displayChipInterviewer = model('');
  readonly displayChipRecruiter = model('');
  readonly displayChipCandidate = model('');

  readonly job = signal([]);
  readonly interviewers = signal([]);
  readonly recruiter = signal([]);
  readonly candidate = signal([]);

  selectedInterviewerValues: string[] = [];
  //#endregion

  /**
   * Constructor
   */
  constructor(
    private location: Location,
    private snackBar: MatSnackBar,
    private router: Router,
    private scheduleService: ScheduleService,
    private authService: AuthService,
    private route: ActivatedRoute,
    private dialog: MatDialog,
    private jobService: JobService,
    private userService: UserService,
    private candidateService: CandidateService
  ) {}

  ngOnInit(): void {
    // Get schedule id from route
    this.scheduleId = this.route.snapshot.paramMap.get('id');

    this.scheduleForm = new FormGroup({
      id: new FormControl(this.scheduleId),
      title: new FormControl('', [
        Validators.required,
        Validators.maxLength(255),
        Validators.minLength(8),
      ]),
      jobId: new FormControl('', [Validators.required]),
      candidateId: new FormControl('', [Validators.required]),
      scheduleDate: new FormControl('', [Validators.required]),
      startTime: new FormControl('', [Validators.required]),
      endTime: new FormControl('', [Validators.required]),
      location: new FormControl(null, [Validators.maxLength(255)]),
      recruiterOwnerId: new FormControl('', [Validators.required]),
      note: new FormControl(null),
      meetingId: new FormControl(null, [Validators.maxLength(255)]),
      interviewerId: new FormControl([]),
      result: new FormControl(null),
    });

    this.scheduleFormCancel = new FormGroup({
      id: new FormControl(this.scheduleId),
    });

    // Get schedule detail by id
    this.scheduleService.getDetail(this.scheduleId).subscribe({
      next: (res) => {
        this.schedule = res;
        console.log(this.schedule);
        // Set form value
        this.scheduleForm.patchValue({
          title: this.schedule.title,
          jobId: this.schedule.jobId,
          candidateId: this.schedule.candidateId,
          scheduleDate: this.schedule.scheduleDate,
          startTime: this.schedule.startTime.slice(0, 5),
          endTime: this.schedule.endTime.slice(0, 5),
          location: this.schedule.location,
          recruiterOwnerId: this.schedule.recruiterOwnerId,
          note: this.schedule.note,
          meetingId: this.schedule.meetingId,
          result: this.schedule.result,
        });

        this.selectedInterviewerValues = this.schedule.interviewers.map(
          (i) => i.id
        );

        this.isCancelAble = this.schedule.status === ScheduleStatusEnum.Open;

        // Set value for chips and autocomplete inputs
        this.job.set([this.schedule.job.title]);
        this.isShowPlaceholderJob = false;
        this.jobInput.nativeElement.disabled = true;

        this.recruiter.set([this.schedule.recruiterOwner.userName]);
        this.isShowPlaceholderRecruiter = false;
        this.recruiterInput.nativeElement.disabled = true;

        this.candidate.set([this.schedule.candidate.fullName]);
        this.isShowPlaceholderCandidate = false;
        this.candidateInput.nativeElement.disabled = true;

        this.interviewers.set(
          this.schedule.interviewers.map((interviewer) => ({
            value: interviewer.id,
            label: interviewer.userName,
          }))
        );
      },

      error: (err) => {
        this.router.navigate(['/not-found']);
      },
    });
  }

  //#region handle submit form
  handleSubmit() {
    if (this.scheduleForm.valid) {
      const formData = this.scheduleForm.value;

      // Update interviewerId with selectedInterviewerValues
      formData.interviewerId = this.selectedInterviewerValues;

      // Convert date to date only in format yyyy-MM-dd
      const scheduleDate = new Date(formData.scheduleDate);
      formData.scheduleDate = scheduleDate.toISOString().split('T')[0];

      // Convert time in format HH:mm to HH:mm:ss if time is in HH:mm format
      if (formData.startTime.length === 5) {
        formData.startTime = `${formData.startTime}:00`;
        formData.endTime = `${formData.endTime}:00`;
      }

      this.scheduleService.updateScheduleDetails(formData).subscribe(
        (response) => {
          if (response.isSuccess) {
            this.snackBar.open('Update schedule successfully', '', {
              duration: 2000,
              horizontalPosition: 'right',
              verticalPosition: 'top',
              panelClass: 'app-notification-success',
            });
            this.router.navigate([
              '/schedule/schedule-details',
              this.scheduleId,
            ]);
          } else {
            this.snackBar.open(response.messageResponse, '', {
              duration: 2000,
              horizontalPosition: 'right',
              verticalPosition: 'top',
              panelClass: 'app-notification-error',
            });
          }
        },
        (error) => {
          let errorMessage =
            error.error.messageResponse ?? 'An unexpected error occurred.';
          if (error.error.errors && error.error.errors.length > 0) {
            // Assuming there's only one error per field for simplicity
            error.error.errors.forEach((fieldError: any) => {
              const fieldName = fieldError.key; // The field name in the form
              const fieldErrorMessage = fieldError.errors[0].errorMessage; // The error message for the field

              // Check if the form has the field
              if (this.scheduleForm.controls[fieldName]) {
                // Set the error message to the form control
                this.scheduleForm.controls[fieldName].setErrors({
                  serverError: fieldErrorMessage,
                });
              }

              // Update the general error message to show the first error
              errorMessage = fieldErrorMessage;
            });
          }

          // Show the error message in the snackBar
          this.snackBar.open(errorMessage, '', {
            duration: 2000,
            horizontalPosition: 'right',
            verticalPosition: 'top',
            panelClass: 'app-notification-error',
          });
        }
      );
    } else {
      console.warn('Form is invalid');
    }
  }
  //#endregion

  //#region Filter data for chips and autocomplete inputs
  // Filter data for jobNames
  readonly filteredJobs = computed(() => {
    const currentJob = this.displayChipJob().toLowerCase();
    this.fetchJobs(currentJob, 1, 1, 10);
    return currentJob
      ? this.jobNames.filter((job) =>
          job.label.toLowerCase().includes(currentJob)
        )
      : this.jobNames.slice();
  });

  // Filter data for interviewerNames
  readonly filteredInterviewers = computed(() => {
    const currentInterviewer = this.displayChipInterviewer().toLowerCase();
    console.log(currentInterviewer);
    this.fetchInterviewer(currentInterviewer, Roles.Interviewer, 1, 10);
    return currentInterviewer
      ? this.interviewerNames.filter((interviewer) =>
          interviewer.label.toLowerCase().includes(currentInterviewer)
        )
      : this.interviewerNames.slice();
  });

  // Filter data for RecruiterNames
  readonly filteredRecruiters = computed(() => {
    const currentRecruiter = this.displayChipRecruiter().toLowerCase();
    this.fetchRecruiter(currentRecruiter, Roles.HR, 1, 10);
    return currentRecruiter
      ? this.RecruiterNames.filter((recruiter) =>
          recruiter.label.toLowerCase().includes(currentRecruiter)
        )
      : this.RecruiterNames.slice();
  });

  // Filter data for candidateNames
  readonly filteredCandidates = computed(() => {
    const currentCandidate = this.displayChipCandidate().toLowerCase();
    this.fetchCandidate(currentCandidate, CandidateStatus.Open, 1, 10);
    return currentCandidate
      ? this.candidateNames.filter((candidate) =>
          candidate.label.toLowerCase().includes(currentCandidate)
        )
      : this.candidateNames.slice();
  });
  //#endregion

  //#region event handlers for chips and autocomplete inputs for job
  @ViewChild('jobInput') jobInput: ElementRef;
  isShowPlaceholderJob: boolean;

  removeJob(jobRemove: string): void {
    // prevent when there is no job
    if (this.isShowPlaceholderJob) {
      return;
    }

    // remove job
    this.job.update((job) => {
      const index = job.indexOf(jobRemove);
      if (index < 0) {
        return job;
      }

      // enable input
      this.isShowPlaceholderJob = true;
      this.jobInput.nativeElement.disabled = false;

      job.splice(index, 1);
      this.updateFormControl('jobId', '');
      return [...job];
    });
  }

  selectedJob(event: MatAutocompleteSelectedEvent): void {
    // prevent when there is exist job
    if (!this.isShowPlaceholderJob) {
      return;
    }

    // add job
    this.job.update((job) => [...job, event.option.viewValue]);
    this.jobInput.nativeElement.value = '';
    this.jobInput.nativeElement.disabled = true;
    this.isShowPlaceholderJob = false;
  }

  selectedJobValue(value: any): void {
    // prevent when there is exist job
    if (!this.isShowPlaceholderJob) {
      return;
    }
    // set value to form control
    this.updateFormControl('jobId', value);
  }
  //#endregion

  //#region event handlers for chips and autocomplete inputs for candidate
  @ViewChild('candidateInput') candidateInput: ElementRef;
  isShowPlaceholderCandidate: boolean;

  removeCandidate(candidateRemove: string): void {
    // prevent when there is no candidate
    if (this.isShowPlaceholderCandidate) {
      return;
    }
    this.candidate.update((candidate) => {
      const index = candidate.indexOf(candidateRemove);
      if (index < 0) {
        return candidate;
      }

      // enable input
      this.isShowPlaceholderCandidate = true;
      this.candidateInput.nativeElement.disabled = false;

      candidate.splice(index, 1);
      this.updateFormControl('candidateId', '');
      return [...candidate];
    });
  }

  selectedCandidate(event: MatAutocompleteSelectedEvent): void {
    // prevent when there is exist candidate
    if (!this.isShowPlaceholderCandidate) {
      return;
    }

    // add candidate
    this.candidate.update((candidate) => [
      ...candidate,
      event.option.viewValue,
    ]);
    this.candidateInput.nativeElement.value = '';
    this.candidateInput.nativeElement.disabled = true;
    this.isShowPlaceholderCandidate = false;
  }

  selectedCandidateValue(value: any): void {
    // prevent when there is exist candidate
    if (!this.isShowPlaceholderCandidate) {
      return;
    }
    // set value to form control
    this.updateFormControl('candidateId', value);
    // get value from form control
    console.log('candidate id' + this.scheduleForm.get('candidateId').value);
  }
  //#endregion

  //#region event handlers for chips and autocomplete inputs for interviewer
  @ViewChild('interviewerInput') interviewerInput: ElementRef;

  removeInterviewer(interviewerRemove: { value: any; label: string }): void {
    this.interviewers.update((interviewer) => {
      const index = interviewer.indexOf(interviewerRemove);
      if (index < 0) {
        return interviewer;
      }

      // Remove the interviewer from the list
      interviewer.splice(index, 1);

      // Assuming selectedInterviewerValues is an array of values corresponding to the interviewers
      // Find the index of the value in selectedInterviewerValues to remove
      const valueIndex = this.selectedInterviewerValues.indexOf(
        interviewerRemove.value
      );

      if (valueIndex > -1) {
        // Remove the value from selectedInterviewerValues
        this.selectedInterviewerValues.splice(valueIndex, 1);
      }

      console.log(this.selectedInterviewerValues);
      this.updateFormControl('interviewerId', this.selectedInterviewerValues);
      console.log(
        'interviewer id form' + this.scheduleForm.get('interviewerId').value
      );

      this.displayChipInterviewer.set('');

      // Check if there no interviewer left prevent submit form and show error message
      if (interviewer.length === 0) {
        this.scheduleForm.controls['interviewerId'].setErrors({
          arrayNotEmpty: true,
        });
      }

      return [...interviewer];
    });
  }

  selectedInterviewer(event: MatAutocompleteSelectedEvent): void {
    // prevent when there is exist interviewer
    if (this.selectedInterviewerValues.includes(event.option.value.value)) {
      return;
    }
    this.selectedInterviewerValues.push(event.option.value.value);
    this.updateFormControl('interviewerId', this.selectedInterviewerValues);

    this.interviewers.update((interviewer) => [
      ...interviewer,
      event.option.value,
    ]);

    this.displayChipInterviewer.set('');
    this.interviewerInput.nativeElement.value = '';
  }
  //#endregion

  //#region event handlers for chips and autocomplete inputs for recruiter
  isShowPlaceholderRecruiter: boolean;
  @ViewChild('recruiterInput') recruiterInput: ElementRef;

  removeRecruiter(recruiterRemove: string): void {
    // prevent when there is no recruiter
    if (this.isShowPlaceholderRecruiter) {
      return;
    }

    this.recruiter.update((recruiter) => {
      const index = recruiter.indexOf(recruiterRemove);
      if (index < 0) {
        return recruiter;
      }

      // enable input
      this.isShowPlaceholderRecruiter = true;
      this.recruiterInput.nativeElement.disabled = false;
      this.recruiterInput.nativeElement.value = '';

      this.updateFormControl('recruiterOwnerId', '');
      recruiter.splice(index, 1);
      return [...recruiter];
    });
  }

  selectedRecruiter(event: MatAutocompleteSelectedEvent): void {
    // prevent when there is exist recruiter
    if (!this.isShowPlaceholderRecruiter) {
      return;
    }

    this.recruiter.update((recruiter) => [
      ...recruiter,
      event.option.viewValue,
    ]);
    this.recruiterInput.nativeElement.value = '';
    this.recruiterInput.nativeElement.disabled = true;
    this.isShowPlaceholderRecruiter = false;
  }

  selectedRecruiterValue(value: any): void {
    // prevent when there is exist recruiter
    if (!this.isShowPlaceholderRecruiter) {
      return;
    }
    // set value to form control
    this.updateFormControl('recruiterOwnerId', value);
  }
  //#endregion

  // trigger when user click cancel button
  goBack() {
    this.location.back();
  }

  // assign recruiter owner id to current user id
  assignMe() {
    const id = this.authService.getUser().userId;
    // check role is recruiter or not
    if (this.authService.getUserRole() == Roles.HR) {
      // prevent when there is exist recruiter
      if (!this.isShowPlaceholderRecruiter) {
        return;
      }

      // add recruiter id by current user id
      this.recruiter.update((recruiter) => [
        ...recruiter,
        this.authService.getUser().username,
      ]);

      // disable input and set value
      this.displayChipRecruiter.set('');
      this.recruiterInput.nativeElement.value = '';
      this.recruiterInput.nativeElement.disabled = true;
      this.isShowPlaceholderRecruiter = false;
      this.updateFormControl('recruiterOwnerId', id);
    } else {
      this.snackBar.open("Your role can't assign", '', {
        duration: 2000,
        horizontalPosition: 'right',
        verticalPosition: 'top',
        panelClass: 'app-notification-error',
      });
    }
  }

  updateFormControl(controlName: string, value: any) {
    const control = this.scheduleForm.get(controlName);
    control.setValue(value);
    control.updateValueAndValidity();
  }

  openConfirmDialog(): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: '120px',
      data: {
        content: 'Are you sure you want to cancel this interview schedule?',
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        const formData = this.scheduleFormCancel.value;
        this.scheduleService.cancelSchedule(formData).subscribe(
          (response) => {
            if (response.isSuccess) {
              this.snackBar.open(response.messageResponse, '', {
                duration: 2000,
                horizontalPosition: 'right',
                verticalPosition: 'top',
                panelClass: 'app-notification-success',
              });
              this.router.navigate([
                '/schedule/schedule-details',
                this.scheduleId,
              ]);
            } else {
              this.snackBar.open(response.messageResponse, '', {
                duration: 2000,
                horizontalPosition: 'right',
                verticalPosition: 'top',
                panelClass: 'app-notification-error',
              });
            }
          },
          (error) => {
            this.snackBar.open(error.error.messageResponse, '', {
              duration: 2000,
              horizontalPosition: 'right',
              verticalPosition: 'top',
              panelClass: 'app-notification-error',
            });
          }
        );
      }
    });
  }

  //#region Fetch data for dropdowns
  fetchJobs(
    search: string,
    status: number,
    pageIndex: number,
    pageSize: number
  ): void {
    this.jobService.getJobs(search, status, pageIndex, pageSize).subscribe(
      (response) => {
        this.jobNames = response.items.map((job) => {
          return { value: job.id, label: job.title };
        });
      },
      (error) => {
        console.error('Error fetching jobs', error);
      }
    );
  }

  fetchInterviewer(
    search: string,
    role: string,
    pageIndex: number,
    pageSize: number
  ): void {
    this.userService
      .getUserByRoleAndUsername(search, role, pageIndex, pageSize)
      .subscribe(
        (response) => {
          this.interviewerNames = response.items.map((iter) => {
            return { value: iter.id, label: iter.userName };
          });
          console.log(this.interviewerNames);
        },
        (error) => {
          console.error('Error fetching iterviewer', error);
        }
      );
  }

  fetchRecruiter(
    search: string,
    role: string,
    pageIndex: number,
    pageSize: number
  ): void {
    this.userService
      .getUserByRoleAndUsername(search, role, pageIndex, pageSize)
      .subscribe(
        (response) => {
          this.RecruiterNames = response.items.map((iter) => {
            return { value: iter.id, label: iter.userName };
          });
          console.log(this.interviewerNames);
        },
        (error) => {
          console.error('Error fetching iterviewer', error);
        }
      );
  }

  fetchCandidate(
    search: string,
    status: number,
    pageIndex: number,
    pageSize: number
  ): void {
    this.candidateService
      .getCandidatesByStatusAndFullName(search, status, pageIndex, pageSize)
      .subscribe(
        (response) => {
          this.candidateNames = response.items.map((candidate) => {
            return { value: candidate.id, label: candidate.fullName };
          });
        },
        (error) => {
          console.error('Error fetching jobs', error);
        }
      );
  }
  //#endregion
}
