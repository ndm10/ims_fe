export enum ContractType {
  TrialTwoMonths = 0,
  TraineeThreeMonths,
  OneYear,
  ThreeYears,
  Unlimited,
}
