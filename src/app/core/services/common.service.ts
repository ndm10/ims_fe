import { Injectable } from '@angular/core';
import { BaseService } from '../http/base-service';
import { Observable, map } from 'rxjs';
import { ResponseData } from '../models/api/response.api.model';
import { ApiPosition } from '../models/api/position.api.model';
import { ApiDepartment } from '../models/api/department.api.model';
import { ApiAttribute } from '../models/api/attribute.api.model';
import { ApiPaths } from '../../../environments/api-paths';

@Injectable({
  providedIn: 'root',
})
export class CommonService {
  constructor(private service: BaseService) {}

  getPositions(): Observable<ApiPosition[]> {
    return this.service
      .get<ResponseData<ApiPosition[]>>(ApiPaths.getPositions)
      .pipe(
        map((response) => {
          if (response.isSuccess) {
            return response.data;
          } else {
            throw new Error(response.messageResponse);
          }
        })
      );
  }

  getDepartments(): Observable<ApiDepartment[]> {
    return this.service
      .get<ResponseData<ApiDepartment[]>>(ApiPaths.getDepartments)
      .pipe(
        map((response) => {
          if (response.isSuccess) {
            return response.data;
          } else {
            throw new Error(response.messageResponse);
          }
        })
      );
  }
  getAllRoles(): Observable<string[]> {
    return this.service.get<ResponseData<string[]>>(ApiPaths.getRoles).pipe(
      map((response) => {
        if (response.isSuccess) {
          return response.data;
        } else {
          throw new Error(response.messageResponse);
        }
      })
    );
  }
  getAttributes(type?: number): Observable<ApiAttribute[]> {
    let types = ``;

    const params = [];
    if (type !== undefined || type > 0) {
      params.push(`attributeTypeEnum=${type}`);
    }
    if (params.length > 0) {
      types += `?${params.join('&')}`;
    }

    return this.service
      .get<ResponseData<ApiAttribute[]>>(ApiPaths.getAttributesByType(types))
      .pipe(
        map((response) => {
          if (response.isSuccess) {
            return response.data;
          } else {
            throw new Error(response.messageResponse);
          }
        })
      );
  }
}
