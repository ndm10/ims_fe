import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../../core/services/auth.service';
import { Router } from '@angular/router';
import { Roles } from '../../../../../environments/roles';
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrl: './sidebar.component.css',
})
export class SidebarComponent implements OnInit {
  userRole: string;
  roles = {
    admin: Roles.Admin,
    hr: Roles.HR,
    manager: Roles.Manager,
    interview: Roles.Interviewer,
  };
  constructor(
    private authService: AuthService,
  ) {}
  ngOnInit(): void {
    this.userRole = this.authService.getUserRole();
  }

}

