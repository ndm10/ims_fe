import { ApiCandidate } from "./candidate.api.model";

export interface ApiListCadidates {
  pageIndex: number;
  totalPages: number;
  pageSize: number;
  totalRecord: number;
  hasPreviousPage: boolean;
  hasNextPage: boolean;
  items: ApiCandidate[];
}
