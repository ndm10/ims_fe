import { Injectable } from '@angular/core';
import { BaseService } from '../http/base-service';
import { ResponseData } from '../models/api/response.api.model';
import { ApiListJobs } from '../models/api/job/list-job.api.model';
import { Observable, map } from 'rxjs';
import { UIListJobs } from '../models/ui/job/list-job.ui.model';
import { AttributeEnum } from '../../shared/enum/attribute.enum';
import { JobStatusMapping } from '../helpers/job-status-mapping';
import { UIJob } from '../models/ui/job/job.ui.model';
import { ApiJob } from '../models/api/job/job.api.model';
import { ImportExcelJob } from '../models/api/job/import-job.api.model';
import { saveAs } from 'file-saver';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root',
})
export class JobService {
  constructor(private service: BaseService, private http: HttpClient) {}
  url = `api/jobs`;
  getJobs(
    search?: string,
    status?: number,
    pageIndex?: number,
    pageSize?: number
  ): Observable<UIListJobs> {
    let params = [];
    if (search !== undefined && search !== '') {
      params.push(`search=${search}`);
    }
    if (status !== undefined && status > 0) {
      params.push(`jobStatus=${status}`);
    }
    if (pageIndex !== undefined && pageIndex > 0) {
      params.push(`pageIndex=${pageIndex}`);
    }
    if (pageSize !== undefined && pageSize > 0) {
      params.push(`pageSize=${pageSize}`);
    }
    // debugger;
    if (params.length > 0) {
      this.url += `?${params.join('&')}`;
    }
    // debugger;
    return this.service.get<ResponseData<ApiListJobs>>(this.url).pipe(
      map((response) => {
        if (response.isSuccess) {
          const jobResponse = response.data;

          const uiListJobs = jobResponse.items.map((job) => {
            const { attributeDTOs, status, ...otherProps } = job;
            return {
              ...otherProps,
              status: JobStatusMapping[status] || 'Unknown',
              skills: attributeDTOs
                .filter((x) => x.type == AttributeEnum.Skills)
                .map((x) => x.value)
                .join(','),
              benefits: attributeDTOs
                .filter((x) => x.type == AttributeEnum.Benefit)
                .map((x) => x.value)
                .join(','),
              levels: attributeDTOs
                .filter((x) => x.type == AttributeEnum.Level)
                .map((x) => x.value)
                .join(','),
            };
          });
          console.log(uiListJobs);
          params = [];
          this.url = `api/jobs`;
          return {
            hasPreviousPage: jobResponse.hasPreviousPage,
            hasNextPage: jobResponse.hasNextPage,
            pageIndex: jobResponse.pageIndex,
            totalPages: jobResponse.totalPages,
            pageSize: jobResponse.pageSize,
            totalRecord: jobResponse.totalRecord,
            items: uiListJobs,
          };
        } else {
          console.log(response.messageResponse);
          throw new Error(response.messageResponse);
        }
      })
    );
  }

  getDetail(id: string): Observable<UIJob> {
    let url = `${this.url}/${id}`;
    return this.service.get<ResponseData<ApiJob>>(url).pipe(
      map((response) => {
        if (response.isSuccess) {
          const apijob = response.data;
          const { attributeDTOs, status, ...otherProps } = apijob;
          return {
            ...otherProps,
            status: JobStatusMapping[status] || 'Unknown',
            skills: attributeDTOs.filter((x) => x.type == AttributeEnum.Skills),
            benefits: attributeDTOs.filter(
              (x) => x.type == AttributeEnum.Benefit
            ),
            levels: attributeDTOs.filter((x) => x.type == AttributeEnum.Level),
          };
        } else {
          throw new Error(response.messageResponse);
        }
      })
    );
  }

  addJob(jobData: any): Observable<any> {
    return this.service.post<any>(this.url, jobData);
  }

  editJob(jobUpdate: UIJob): Observable<boolean> {
    return this.service.put<any>(this.url, jobUpdate);
  }

  deleteJob(id: string): Observable<ResponseData<string>> {
    let url = `${this.url}/?id=${id}`;
    return this.service.delete(url);
  }

  importExcel(file: File): Observable<ResponseData<ImportExcelJob>> {
    let url = `${this.url}/excel/import`;
    const fd = new FormData();
    fd.append('file', file, file.name);
    return this.service.postFile(url, fd);
  }

  downloadFile(excelFileName: string): void {
    const url = `http://171.244.61.201:7080/api/jobs/excel/validated?excelFileName=${excelFileName}`;

    this.http.get(url, { responseType: 'blob' }).subscribe(
      (blob: Blob) => {
        const fileName = excelFileName;
        console.log(fileName);
        saveAs(blob, fileName);
      },
      (error) => {
        console.error('Error downloading file', error);
      }
    );
  }

  downloadExcelTemplate() {
    const url = `http://171.244.61.201:7080/api/jobs/excel/template`;

    this.http.get(url, { responseType: 'blob' }).subscribe(
      (blob: Blob) => {
        const fileName = 'excel_template.xlsx';
        saveAs(blob, fileName);
      },
      (error) => {
        console.error('Error downloading file', error);
      }
    );
  }
}
