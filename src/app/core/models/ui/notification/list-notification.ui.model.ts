import { UINotification } from "./notification.ui.model";

export interface UIListNotification {
  pageIndex: number;
  totalPages: number;
  pageSize: number;
  totalRecord: number;
  hasPreviousPage: boolean;
  hasNextPage: boolean;
  items: UINotification[];
}