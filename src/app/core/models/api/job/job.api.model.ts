import { ApiAttribute } from '../attribute.api.model';

export interface ApiJob {
  id: string;
  title: string;
  attributeDTOs: ApiAttribute[];
  startDate: Date;
  endDate: Date;
  salaryMin: number;
  salaryMax: number;
  address: string;
  description: string;
  status: string;
  createdAt: Date;
  createdBy: string;
  updateAt: Date;
  updateBy: string;
}
