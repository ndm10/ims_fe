import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { ListUserComponent } from './list-user/list-user.component';
import { SharedModule } from '../../shared/shared.module';
import { DetailUserComponent } from './detail-user/detail-user.component';
import { DatePipe } from '@angular/common';
import { AddUserComponent } from './add-user/add-user.component';
import { EditUserComponent } from './edit-user/edit-user.component'; 

@NgModule({
  declarations: [
    ListUserComponent,
    DetailUserComponent,
    AddUserComponent,
    EditUserComponent
  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    SharedModule
  ],
  providers: [DatePipe],
})
export class UserModule { }
