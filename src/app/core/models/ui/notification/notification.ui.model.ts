export interface UINotification {
    id: string;
    title: string;
    content: string;
    toUserId: string;
    urlRedirect: Date;
    isRead: boolean;
    createdAt: string;
  }
  