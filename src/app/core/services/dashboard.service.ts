import { Injectable } from '@angular/core';
import { BaseService } from '../http/base-service';
import { Observable } from 'rxjs';
import { ResponseData } from '../models/api/response.api.model';
import { ApiCardItem } from '../models/api/card-item.api.model';
import { ApiPaths } from '../../../environments/api-paths';
import { ApiChart } from '../models/api/chart.api.mode';

@Injectable({
  providedIn: 'root',
})
export class DashboardService {
  constructor(private service: BaseService) {}

  // getCardItems() : Observable<ResponseData<ApiCardItem>> {
  //   return this.service.get<ResponseData<ApiCardItem>(ApiPaths.banCandidate);
  // }

  getCardItems(): Observable<ResponseData<ApiCardItem[]>> {
    return this.service.get(ApiPaths.getCardItems);
  }

  countInterviews(dateType: number): Observable<ResponseData<ApiChart[]>> {
    return this.service.get(ApiPaths.countInterviews(dateType));
  }

  successCandidates(dateType: number): Observable<ResponseData<ApiChart[]>> {
    return this.service.get(ApiPaths.successCandidates(dateType));
  }
}
