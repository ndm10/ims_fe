import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {
  HttpClientModule,
  provideHttpClient,
  withInterceptors,
} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CandidateModule } from './modules/candidate/candidate.module';
import { JobModule } from './modules/job/job.module';
import { OfferModule } from './modules/offer/offer.module';
import { ScheduleModule } from './modules/schedule/schedule.module';
import { UserModule } from './modules/user/user.module';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { SharedModule } from './shared/shared.module';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';
import { AuthModule } from './modules/auth/auth.module';
import { AuthInterceptor } from './core/interceptors/auth.interceptor';
import { DashboardModule } from './modules/dashboard/dashboard.module';
import { CookieService } from 'ngx-cookie-service';

@NgModule({
  //các component, directive và pipe tạo ra trong module đó
  declarations: [AppComponent],
  //Các module phụ thuộc
  imports: [
    SharedModule,
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    CandidateModule,
    JobModule,
    OfferModule,
    ScheduleModule,
    UserModule,
    DashboardModule,
    AuthModule,
    HttpClientModule,
    NgMultiSelectDropDownModule.forRoot(),
  ],

  //Thêm dịch vụ/guard
  providers: [
    CookieService,
    provideAnimationsAsync(),
    provideHttpClient(withInterceptors([AuthInterceptor])),
  ],
  //component chạy đầu tiên khi hệ thống chạy
  bootstrap: [AppComponent],
})
export class AppModule {}
