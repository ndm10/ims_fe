import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListJobComponent } from './list-job/list-job.component';
import { DetailJobComponent } from './detail-job/detail-job.component';
import { AddJobComponent } from './add-job/add-job.component';
import { UpdateJobComponent } from './update-job/update-job.component';
import { ImportJobComponent } from './import-job/import-job.component';
import { authGuard } from '../../core/guards/auth.guard';
import { Roles } from '../../../environments/roles';

const routes: Routes = [
  { path: '', redirectTo: 'list-job', pathMatch: 'full' },
  {
    path: 'list-job',
    component: ListJobComponent,
    canActivate: [authGuard],
    data: {
      breadcrumb: 'List Jobs',
      roles: [Roles.Admin, Roles.HR, Roles.Interviewer, Roles.Manager],
    },
  },
  {
    path: 'detail-job/:id',
    component: DetailJobComponent,
    canActivate: [authGuard],
    data: {
      breadcrumb: 'Detail Jobs',
      roles: [Roles.Admin, Roles.HR, Roles.Manager],
    },
  },
  {
    path: 'add-job',
    component: AddJobComponent,
    canActivate: [authGuard],
    data: {
      breadcrumb: 'Add Jobs',
      roles: [Roles.Admin, Roles.HR, Roles.Manager],
    },
  },
  {
    path: 'edit-job/:id',
    component: UpdateJobComponent,
    canActivate: [authGuard],
    data: {
      breadcrumb: 'Edit Jobs',
      roles: [Roles.Admin, Roles.HR, Roles.Manager],
    },
  },
  {
    path: 'import-job',
    component: ImportJobComponent,
    canActivate: [authGuard],
    data: {
      breadcrumb: 'Import Jobs',
      roles: [Roles.Admin, Roles.HR, Roles.Manager],
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class JobRoutingModule {}
