import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OfferService } from '../../../core/services/offer.service';
import { ApiOffer } from '../../../core/models/api/offer/offer.api.model';
import { AuthService } from '../../../core/services/auth.service';
import { switchMap } from 'rxjs';

@Component({
  selector: 'app-detail-offer',
  templateUrl: './detail-offer.component.html',
  styleUrl: './detail-offer.component.css',
})
export class DetailOfferComponent implements OnInit {
  id: string;
  offer?: ApiOffer;
  userRole: string;
  interviewersName: string;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private offerService: OfferService,
    private authService: AuthService
  ) {}
  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.offerService.getDetail(this.id).subscribe({
      next: (res) => {
        this.offer = res;
        this.interviewersName = res?.schedule?.interviewers
          .map((i) => i.fullName)
          .join(', ');
      },
      error: (err) => {
        this.router.navigate(['/not-found']);
      },
    });
    this.userRole = this.authService.getUserRole();
  }
  handleAction(
    action:
      | 'approve'
      | 'reject'
      | 'cancel'
      | 'sentToCandidate'
      | 'accept'
      | 'decline',
    id: string
  ) {
    let actionObservable;

    switch (action) {
      case 'approve':
        actionObservable = this.offerService.approveOffer(this.id);
        break;
      case 'reject':
        actionObservable = this.offerService.rejectOffer(this.id);
        break;
      case 'cancel':
        actionObservable = this.offerService.cancelOffer(this.id);
        break;
      case 'sentToCandidate':
        actionObservable = this.offerService.sentCandidateOffer(this.id);
        break;
      case 'accept':
        actionObservable = this.offerService.acceptOffer(this.id);
        break;
      case 'decline':
        actionObservable = this.offerService.declineOffer(this.id);
        break;
      default:
        return;
    }

    actionObservable
      .pipe(switchMap(() => this.offerService.getDetail(this.id)))
      .subscribe({
        next: (res) => {
          this.offer = res;
        },
      });
  }
}
