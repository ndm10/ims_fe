export interface ApiUser {
    id: string;
    userName: string;
    email: string;
    fullName: string;
    dateOfBirth?: Date;
    address: string;
    gender: string;
    note?: string; 
    departmentId: string; 
    departmentName?: string;
    roleName: string;
    status: string;
    phoneNumber: string;
}
