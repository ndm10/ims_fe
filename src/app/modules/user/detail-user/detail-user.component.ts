import { Component, OnInit } from '@angular/core';
import { ApiUser } from '../../../core/models/api/user/user.api.model';
import { UserService } from '../../../core/services/user.service';
import { ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ConfirmDialogComponent } from '../../../shared/components/Dialog/confirm-dialog/confirm-dialog.component';
import { LockUnlockRequest } from '../../../core/models/api/user/lock-unlock.request.model';

@Component({
  selector: 'app-detail-user',
  templateUrl: './detail-user.component.html',
  styleUrl: './detail-user.component.css'
})
export class DetailUserComponent implements OnInit {
  id: string;
  user: ApiUser;
  lockUnlockRequest: LockUnlockRequest;
  
  constructor(
    private userService: UserService,
    private route: ActivatedRoute,
    private datePipe: DatePipe,
    private dialog: MatDialog,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.userService.getDetail(this.id).subscribe(
      (response) => {
        this.user = response;
      },
      (error) => {
        //Should go to page error
        console.error('Error fetching user', error);
      }
    );
  }
  getFormattedDate() {
    if (this.user?.dateOfBirth) {
      return this.datePipe.transform(this.user.dateOfBirth, 'dd/MM/yyyy');
    } else {
      return '';
    }
  }
  openConfirmDialog(): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      height: '120px',
      data: { content: 'Are you sure you want to update this user?' },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.lockUnlockRequest = { uid: this.user.id };
        this.userService.lockUnlockUser(this.lockUnlockRequest).subscribe(
          (response) => {
            if (response.isSuccess) {
              this.user.status = response.data.status;
              this.snackBar.open(response.messageResponse, '', {
                duration: 2000,
                horizontalPosition: 'right',
                verticalPosition: 'top',
                panelClass: 'app-notification-success',
              });
            } else {
              this.snackBar.open(response.messageResponse, '', {
                duration: 2000,
                horizontalPosition: 'right',
                verticalPosition: 'top',
                panelClass: 'app-notification-error',
              });
            }
          },
          (error) => {
            this.snackBar.open(error.error.Errors[0], '', {
              duration: 2000,
              horizontalPosition: 'right',
              verticalPosition: 'top',
              panelClass: 'app-notification-error',
            });
          }
        );
      }
    });
  }
}
