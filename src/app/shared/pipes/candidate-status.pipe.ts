import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'candidateStatus',
})
export class CandidateStatusPipe implements PipeTransform {
  transform(apiStatus: number): string {
    switch (apiStatus) {
      case 1:
        return 'Waiting for interview';
      case 2:
        return 'Waiting for approval';
      case 3:
        return 'Waiting for response';
      case 4:
        return 'Open';
      case 5:
        return 'Passed interview';
      case 6:
        return 'Approved offer';
      case 7:
        return 'Rejected offer';
      case 8:
        return 'Accepted offer';
      case 9:
        return 'Declined offer';
      case 10:
        return 'Cancelled offer';
      case 11:
        return 'Failed interview';
      case 12:
        return 'Cancelled interview';
      case 13:
        return 'Banned';
      default:
        return 'Unknown';
    }
  }
}
