import { ApiSchedule } from './schedule.api.model';

export interface ApiListSchedule {
  pageIndex: number;
  totalPages: number;
  pageSize: number;
  totalRecord: number;
  hasPreviousPage: boolean;
  hasNextPage: boolean;
  items: ApiSchedule[];
}
